/**
 * This file is a part of Papyrus.
 * A Research Paper Manager
 * Copyright 2018-2022 Marcus Britanicus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, vsit http://www.gnu.org/licenses/.
 **/

#include <unistd.h>

/** DFL::Application */
#include <DFApplication.hpp>

#include "Global.hpp"
#include "UI.hpp"

#include "PapersViewDelegate.hpp"
#include "PapersModel.hpp"
#include "PaperSearch.hpp"
#include "AddPaper.hpp"

QSettings   *papSett   = nullptr;
QSettings   *papersDB  = nullptr;
QString     dbPath     = QString();
QStringList papersPath = {};
PaperSearch *doiSearch = nullptr;

int main( int argc, char **argv ) {
    DFL::Application app( argc, argv );

    /* About Application */
    app.setOrganizationName( "Papyrus" );
    app.setApplicationName( "Papyrus" );
    app.setDesktopFileName( "papyrus" );

    app.setQuitOnLastWindowClosed( false );

    if ( app.isRunning() ) {
        app.messageServer( "show" );

        app.quit();
        return 0;
    }

    if ( not app.lockApplication() ) {
        QMessageBox::information(
            NULL,
            "Papyrus",
            "Unable to acquire single instance lock. Multiple instances of this application may start."
        );
    }

    papSett    = new QSettings( "Papyrus", "Papyrus" );
    dbPath     = papSett->value( "dbPath" ).toString();
    papersPath = papSett->value( "papersPath" ).toStringList();

    papersDB  = new QSettings( QDir( dbPath ).filePath( "papers.db" ), QSettings::NativeFormat );
    doiSearch = new PaperSearch();

    if ( dbPath.isEmpty() ) {
        QMessageBox::information(
            NULL,
            "Papyrus",
            "Database path has not been set. Please set the database path now."
        );

        QString dbPath = QFileDialog::getExistingDirectory(
            NULL,
            "Papyrus | Database Path",
            QDir::homePath()
        );

        if ( dbPath.isEmpty() ) {
            return 1;
        }

        papSett->setValue( "dbPath", dbPath );
        papSett->sync();
    }

    if ( papersPath.isEmpty() ) {
        QMessageBox::information(
            NULL,
            "Papyrus",
            "The place where you save your PDFs is not known to papyrus. "
            "Please choose the directory where you store your PDFs."
        );

        QString papersPath = QFileDialog::getExistingDirectory(
            NULL,
            "Papyrus | Papers Path",
            QDir::homePath()
        );

        if ( papersPath.isEmpty() ) {
            return 1;
        }

        papSett->setValue( "papersPath", QStringList() << papersPath );
        papSett->sync();
    }

    QSettings symbols( "Papyrus", "SymbolsChart" );

    if ( not QFileInfo( symbols.fileName() ).exists() ) {
        QFile::copy( "/usr/share/papyrus/SymbolsChart.conf", symbols.fileName() );
    }

    else if ( not symbols.allKeys().count() ) {
        QFile::copy( "/usr/share/papyrus/SymbolsChart.conf", symbols.fileName() );
    }

    QStringList addedPapers = papersDB->value( "AddedPaths" ).toStringList();

    addedPapers.sort();

    QStringList unadded, nonexist;

    Q_FOREACH ( QString pPath, papersPath ) {
        QDir        pDir   = QDir( pPath );
        QStringList papers = pDir.entryList( QStringList() << "*.pdf", QDir::Files );
        papers.sort();

        for ( int i = 0; i < papers.count(); i++ ) {
            papers[ i ] = pDir.filePath( papers[ i ] );
        }

        Q_FOREACH ( QString paper, papers ) {
            if ( not addedPapers.contains( paper ) ) {
                unadded << paper;
            }
        }

        Q_FOREACH ( QString paper, addedPapers ) {
            if ( not papers.contains( paper ) ) {
                nonexist << paper;
            }
        }
    }

    for ( QString ppr: unadded ) {
        qDebug() << "New:" << ppr;
    }

    for ( QString ppr: nonexist ) {
        qDebug() << "NE:" << ppr;
    }

    if ( unadded.count() + nonexist.count() ) {
        QString unaddedStr = QString(
            " There are %1 paper%2 which have not yet been added to the database."
        ).arg( unadded.count() ).arg( unadded.count() > 1 ? "s" : "" );
        QString nonexistStr = QString(
            " There are %1 paper%2 which do not exist in the given paths."
        ).arg( nonexist.count() ).arg( nonexist.count() > 1 ? "s" : "" );

        int response = QMessageBox::question(
            NULL,
            "Papyrus | Database",
            QString(
                "Your database seems to be outdated.%1%2 Would you like to fix these inconsistencies?"
            ).arg( unadded.count() ? unaddedStr : "" ).arg( nonexist.count() ? nonexistStr : "" ),
            QMessageBox::Yes | QMessageBox::No
        );

        if ( response == QMessageBox::Yes ) {
            AddPapersManager *addPapersManager = new AddPapersManager();
            addPapersManager->addPapers( unadded );
        }
    }

    UI *Gui = new UI();

    QObject::connect(
        &app, &DFL::Application::messageFromClient, [ Gui ] ( const QString&, int ) {
            Gui->show();
        }
    );

    Gui->showMaximized();

    return app.exec();
}
