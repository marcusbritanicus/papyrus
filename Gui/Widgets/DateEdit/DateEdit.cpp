/**
 * This file is a part of Papyrus.
 * A Research Paper Manager
 * Copyright 2018-2022 Marcus Britanicus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, vsit http://www.gnu.org/licenses/.
 **/

#include "DateEdit.hpp"

DateEdit::DateEdit( QWidget *parent ) : QDateEdit( parent ) {
    setDisplayFormat( "MMMM yyyy" );
    setCalendarPopup( true );

    /* Default date is Jan 10, 2000 */
    mIsDateSet = false;
    connect( this, SIGNAL(dateChanged(const QDate&)), SLOT(resetDate(const QDate&)) );
}


bool DateEdit::isDateSet() {
    return mIsDateSet;
}


void DateEdit::resetDate( const QDate& ) {
    mIsDateSet = true;
}


void DateEdit::setEnabled( bool enabled ) {
    QDateEdit::setEnabled( enabled );

    if ( not enabled ) {
        mIsDateSet = false;
    }
}


void DateEdit::setDisabled( bool disabled ) {
    QDateEdit::setDisabled( disabled );

    if ( disabled ) {
        mIsDateSet = false;
    }
}


void DateEdit::focusInEvent( QFocusEvent *fEvent ) {
    Qt::FocusReason reason = fEvent->reason();

    if ( reason == Qt::PopupFocusReason ) {
        return;
    }

    QPointF point = rect().bottomRight() - QPointF{ 5, 16 };

    QCoreApplication::postEvent(
        this,
        new QMouseEvent(
            QEvent::MouseButtonPress,
            point,
            Qt::LeftButton,
            Qt::LeftButton,
            Qt::NoModifier
        )
    );

    QDateEdit::focusInEvent( fEvent );
}


void DateEdit::focusOutEvent( QFocusEvent *fEvent ) {
    Qt::FocusReason reason = fEvent->reason();

    if ( reason == Qt::PopupFocusReason ) {
        return;
    }

    if ( calendarWidget()->isVisible() ) {
        calendarWidget()->close();
    }

    QDateEdit::focusOutEvent( fEvent );
}
