/**
 * This file is a part of Papyrus.
 * A Research Paper Manager
 * Copyright 2018-2022 Marcus Britanicus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, vsit http://www.gnu.org/licenses/.
 **/

#include "RichTextEdit.hpp"

RichTextEdit::RichTextEdit( QWidget *parent ) : QTextEdit( parent ) {
    /* Font */
    setFont( QFont( "LM Sans 8", 11 ) );

    /* Disable readOnly */
    setReadOnly( false );

    /* Enable rich text */
    setAcceptRichText( true );

    /* Word wrapping */
    setWordWrapMode( QTextOption::WordWrap );

    /* Text Alignment */
    setAlignment( Qt::AlignJustify );

    /* Tab changes focus */
    setTabChangesFocus( true );

    /* Focus Policy */
    setFocusPolicy( Qt::StrongFocus );

    /* Action: Bold */
    QAction *boldAct = new QAction( "Bold", this );

    boldAct->setShortcut( tr( "Ctrl+B" ) );
    boldAct->setShortcutContext( Qt::WidgetShortcut );
    connect( boldAct, SIGNAL(triggered()), SLOT(embolden()) );
    addAction( boldAct );

    /* Action: Italic */
    QAction *italicAct = new QAction( "Italic", this );

    italicAct->setShortcut( tr( "Ctrl+I" ) );
    italicAct->setShortcutContext( Qt::WidgetShortcut );
    connect( italicAct, SIGNAL(triggered()), SLOT(italicize()) );
    addAction( italicAct );

    /* Action: Underline */
    QAction *underlineAct = new QAction( "Underline", this );

    underlineAct->setShortcut( tr( "Ctrl+U" ) );
    underlineAct->setShortcutContext( Qt::WidgetShortcut );
    connect( underlineAct, SIGNAL(triggered()), SLOT(underline()) );
    addAction( underlineAct );

    /* Action: Overline */
    QAction *overlineAct = new QAction( "Overline", this );

    overlineAct->setShortcut( tr( "Ctrl+Shift+U" ) );
    overlineAct->setShortcutContext( Qt::WidgetShortcut );
    connect( overlineAct, SIGNAL(triggered()), SLOT(overline()) );
    addAction( overlineAct );

    /* Action: SuperScript */
    QAction *superScriptAct = new QAction( "SuperScript", this );

    superScriptAct->setShortcut( tr( "Alt+Shift+Up" ) );
    superScriptAct->setShortcutContext( Qt::WidgetShortcut );
    connect( superScriptAct, SIGNAL(triggered()), SLOT(superScript()) );
    addAction( superScriptAct );

    /* Action: SubScript */
    QAction *subScriptAct = new QAction( "SubScript", this );

    subScriptAct->setShortcut( tr( "Alt+Shift+Down" ) );
    subScriptAct->setShortcutContext( Qt::WidgetShortcut );
    connect( subScriptAct, SIGNAL(triggered()), SLOT(subScript()) );
    addAction( subScriptAct );

    /* Action: Select All */
    QAction *selectAllAct = new QAction( "Select All", this );

    selectAllAct->setShortcut( tr( "Ctrl+A" ) );
    selectAllAct->setShortcutContext( Qt::WidgetShortcut );
    connect( selectAllAct, SIGNAL(triggered()), SLOT(selectAll()) );
    addAction( selectAllAct );

    /* Action: Deselect All */
    QAction *deselectAllAct = new QAction( "Deselect All", this );

    deselectAllAct->setShortcut( tr( "Ctrl+Shift+A" ) );
    deselectAllAct->setShortcutContext( Qt::WidgetShortcut );
    connect( deselectAllAct, SIGNAL(triggered()), SLOT(deselectAll()) );
    addAction( deselectAllAct );

    /* Action: EnglishToGreek */
    QAction *engToGreekAct = new QAction( "Letter to Greek", this );

    engToGreekAct->setShortcut( tr( "Ctrl+G" ) );
    engToGreekAct->setShortcutContext( Qt::WidgetShortcut );
    connect( engToGreekAct, SIGNAL(triggered()), SLOT(englishToGreek()) );
    addAction( engToGreekAct );

    /* Action: CodeToSymbol */
    QAction *codeToSymbAct = new QAction( "Code to Symbol", this );

    codeToSymbAct->setShortcut( tr( "Ctrl+Shift+S" ) );
    codeToSymbAct->setShortcutContext( Qt::WidgetShortcut );
    connect( codeToSymbAct, SIGNAL(triggered()), SLOT(codeToSymbols()) );
    addAction( codeToSymbAct );
}


void RichTextEdit::paste() {
    insertFromMimeData( QApplication::clipboard()->mimeData() );
}


void RichTextEdit::mergeFormatOnSelection( const QTextCharFormat& format ) {
    QTextCursor cursor = textCursor();

    if ( not cursor.hasSelection() ) {
        cursor.select( QTextCursor::WordUnderCursor );
    }

    cursor.mergeCharFormat( format );
}


void RichTextEdit::embolden() {
    QTextCursor cursor = textCursor();

    if ( not cursor.hasSelection() ) {
        cursor.select( QTextCursor::WordUnderCursor );
    }

    int start = cursor.selectionStart();
    int end   = cursor.selectionEnd();

    cursor.setPosition( start );
    cursor.setPosition( end, QTextCursor::KeepAnchor );

    QTextCharFormat fmt = cursor.charFormat();

    fmt.setFontWeight( fmt.fontWeight() > QFont::Normal ? QFont::Bold : QFont::Normal );
    cursor.mergeCharFormat( fmt );
}


void RichTextEdit::italicize() {
    QTextCursor cursor = textCursor();

    if ( not cursor.hasSelection() ) {
        cursor.select( QTextCursor::WordUnderCursor );
    }

    int start = cursor.selectionStart();
    int end   = cursor.selectionEnd();

    cursor.setPosition( start );
    cursor.setPosition( end, QTextCursor::KeepAnchor );

    QTextCharFormat fmt = cursor.charFormat();

    fmt.setFontItalic( fmt.fontItalic() ? false : true );
    cursor.mergeCharFormat( fmt );
}


void RichTextEdit::underline() {
    QTextCursor cursor = textCursor();

    if ( not cursor.hasSelection() ) {
        cursor.select( QTextCursor::WordUnderCursor );
    }

    int start = cursor.selectionStart();
    int end   = cursor.selectionEnd();

    cursor.setPosition( start );
    cursor.setPosition( end, QTextCursor::KeepAnchor );

    QTextCharFormat fmt = cursor.charFormat();

    fmt.setFontUnderline( fmt.fontUnderline() ? false : true );
    cursor.mergeCharFormat( fmt );
}


void RichTextEdit::overline() {
    QTextCursor cursor = textCursor();

    if ( not cursor.hasSelection() ) {
        cursor.select( QTextCursor::WordUnderCursor );
    }

    int start = cursor.selectionStart();
    int end   = cursor.selectionEnd();

    cursor.setPosition( start );
    cursor.setPosition( end, QTextCursor::KeepAnchor );

    QTextCharFormat fmt = cursor.charFormat();

    fmt.setFontOverline( fmt.fontOverline() ? false : true );
    cursor.mergeCharFormat( fmt );
}


void RichTextEdit::superScript() {
    QTextCursor cursor = textCursor();

    if ( not cursor.hasSelection() ) {
        cursor.select( QTextCursor::WordUnderCursor );
    }

    int start = cursor.selectionStart();
    int end   = cursor.selectionEnd();

    cursor.setPosition( start );
    cursor.setPosition( end, QTextCursor::KeepAnchor );

    QTextCharFormat fmt = cursor.charFormat();

    fmt.setVerticalAlignment( fmt.verticalAlignment() == QTextCharFormat::AlignSuperScript ? QTextCharFormat::AlignNormal : QTextCharFormat::AlignSuperScript );
    cursor.mergeCharFormat( fmt );
}


void RichTextEdit::subScript() {
    QTextCursor cursor = textCursor();

    if ( not cursor.hasSelection() ) {
        cursor.select( QTextCursor::WordUnderCursor );
    }

    int start = cursor.selectionStart();
    int end   = cursor.selectionEnd();

    cursor.setPosition( start );
    cursor.setPosition( end, QTextCursor::KeepAnchor );

    QTextCharFormat fmt = cursor.charFormat();

    fmt.setVerticalAlignment( fmt.verticalAlignment() == QTextCharFormat::AlignSubScript ? QTextCharFormat::AlignNormal : QTextCharFormat::AlignSubScript );
    cursor.mergeCharFormat( fmt );
}


void RichTextEdit::selectAll() {
    QTextCursor cursor = textCursor();

    cursor.select( QTextCursor::Document );
}


void RichTextEdit::deselectAll() {
    QTextCursor cursor = textCursor();

    cursor.clearSelection();
}


void RichTextEdit::englishToGreek() {
    QTextCursor cursor = textCursor();

    if ( not cursor.hasSelection() ) {
        return;
    }

    static QMap<QString, QString> alphabetChart;

    alphabetChart[ "a" ] = QString( "&alpha;" );
    alphabetChart[ "b" ] = QString( "&beta;" );
    alphabetChart[ "c" ] = QString( "&chi;" );
    alphabetChart[ "d" ] = QString( "&delta;" );
    alphabetChart[ "e" ] = QString( "&epsilon;" );
    alphabetChart[ "f" ] = QString( "&phi;" );
    alphabetChart[ "g" ] = QString( "&gamma;" );
    alphabetChart[ "h" ] = QString( "&eta;" );
    alphabetChart[ "i" ] = QString( "&iota;" );
    // alphabetChart[ "j" ] = QString( "j" )			Does not exist
    alphabetChart[ "k" ] = QString( "&kappa;" );
    alphabetChart[ "l" ] = QString( "&lambda;" );
    alphabetChart[ "m" ] = QString( "&mu;" );
    alphabetChart[ "n" ] = QString( "&nu;" );
    alphabetChart[ "o" ] = QString( "&omicron;" );
    alphabetChart[ "p" ] = QString( "&pi;" );
    alphabetChart[ "q" ] = QString( "&theta;" );
    alphabetChart[ "r" ] = QString( "&rho;" );
    alphabetChart[ "s" ] = QString( "&sigma;" );
    alphabetChart[ "t" ] = QString( "&tau;" );
    alphabetChart[ "u" ] = QString( "&upsilon;" );
    // alphabetChart[ "v" ] = QString( "v" )			Does not exist
    alphabetChart[ "w" ] = QString( "&omega;" );
    alphabetChart[ "x" ] = QString( "&xi;" );
    alphabetChart[ "y" ] = QString( "&psi;" );
    alphabetChart[ "z" ] = QString( "&zeta;" );

    alphabetChart[ "A" ] = QString( "&Alpha;" );
    alphabetChart[ "B" ] = QString( "&Beta;" );
    alphabetChart[ "C" ] = QString( "&Chi;" );
    alphabetChart[ "D" ] = QString( "&Delta;" );
    alphabetChart[ "E" ] = QString( "&Epsilon;" );
    alphabetChart[ "F" ] = QString( "&Phi;" );
    alphabetChart[ "G" ] = QString( "&Gamma;" );
    alphabetChart[ "H" ] = QString( "&Eta;" );
    alphabetChart[ "I" ] = QString( "&Iota;" );
    // alphabetChart[ "J" ] = QString( "J" )			Does not exist
    alphabetChart[ "K" ] = QString( "&Kappa;" );
    alphabetChart[ "L" ] = QString( "&Lambda;" );
    alphabetChart[ "M" ] = QString( "&Mu;" );
    alphabetChart[ "N" ] = QString( "&Nu;" );
    alphabetChart[ "O" ] = QString( "&Omicron;" );
    alphabetChart[ "P" ] = QString( "&Pi;" );
    alphabetChart[ "Q" ] = QString( "&Theta;" );
    alphabetChart[ "R" ] = QString( "&Rho;" );
    alphabetChart[ "S" ] = QString( "&Sigma;" );
    alphabetChart[ "T" ] = QString( "&Tau;" );
    alphabetChart[ "U" ] = QString( "&Upsilon;" );
    // alphabetChart[ "V" ] = QString( "V" )			Does not exist
    alphabetChart[ "W" ] = QString( "&Omega;" );
    alphabetChart[ "X" ] = QString( "&Xi;" );
    alphabetChart[ "Y" ] = QString( "&Psi;" );
    alphabetChart[ "Z" ] = QString( "&Zeta;" );

    alphabetChart[ "q*" ] = QString( "&thetasym;" );
    alphabetChart[ "w*" ] = QString( "&piv;" );
    alphabetChart[ "f*" ] = QString( "&straightphi;" );
    alphabetChart[ "r*" ] = QString( "&varrho;" );
    alphabetChart[ "e*" ] = QString( "&straightepsilon;" );

    QString sel = cursor.selectedText();
    QString newText;

    Q_FOREACH ( QChar ch, sel ) {
        newText += alphabetChart.value( QString( ch ), QString( ch ) );
    }

    cursor.insertHtml( newText );
}


void RichTextEdit::codeToSymbols() {
    QTextCursor cursor = textCursor();

    if ( not cursor.hasSelection() ) {
        return;
    }

    QSettings symbolsChart( "/usr/share/papyrus/SymbolsChart.conf", QSettings::NativeFormat );

    QString sel     = cursor.selectedText();
    QString newText = symbolsChart.value( sel, sel ).toString();

    cursor.insertHtml( newText );
}


void RichTextEdit::insertFromMimeData( const QMimeData *source ) {
    if ( not source ) {
        return;
    }

    QString txt, fragment;

    if ( source->hasFormat( "application/x-qrichtext" ) ) {
        txt = QString::fromUtf8( source->data( "application/x-qrichtext" ) );
    }

    else if ( source->hasHtml() ) {
        txt = source->html().replace( "&nbsp;", " " );
    }

    else if ( source->hasText() ) {
        txt = source->text().simplified();
    }

    txt = txt.simplified();

    QRegularExpression reHead(
        "<head.*head>",
        QRegularExpression::CaseInsensitiveOption | QRegularExpression::InvertedGreedinessOption
    );
    QRegularExpression reBlock(
        QRegularExpression(
            "<[//]{0,1}(div|p|h1|h2|h3|h4|h5|h6|br|hr|body|html|head|!doctype)[^><]*>",
            QRegularExpression::CaseInsensitiveOption | QRegularExpression::InvertedGreedinessOption
        )
    );

    fragment = txt.replace( reHead, "" ).replace( reBlock, "" );

    textCursor().insertHtml( fragment );

    ensureCursorVisible();
}


RichTextBrowser::RichTextBrowser( QWidget *parent ) : QTextBrowser( parent ) {
    /* Font */
    setFont( QFont( "LM Sans 8", 13 ) );

    /* Enable rich text */
    setAcceptRichText( true );

    /* Word wrapping */
    setWordWrapMode( QTextOption::WordWrap );

    /* Text Alignment */
    setAlignment( Qt::AlignJustify );

    /* Tab changes focus */
    setTabChangesFocus( true );
}
