/**
 * This file is a part of Papyrus.
 * A Research Paper Manager
 * Copyright 2018-2022 Marcus Britanicus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, vsit http://www.gnu.org/licenses/.
 **/

#include "SearchBar.hpp"
#include "StyleSheet.h"

SearchBar::SearchBar( QWidget *parent ) : QWidget( parent ) {
    /** Create the user interface */
    createUI();

    /** Set up the connections */
    setupConnections();

    /** Search Timer */
    searchTimer = new QBasicTimer();
}


void SearchBar::createUI() {
    authorLbl = new QLabel();
    authorLE  = new QLineEdit();

    textLbl = new QLabel();
    textLE  = new QLineEdit();

    dateLbl = new QLabel();
    fromDE  = new DateEdit();

    toLbl = new QLabel( "&to" );
    toDE  = new DateEdit();

    separator = new QWidget();

    clearBtn = new QToolButton();

    addPaperBtn = new QToolButton();
    getPaperBtn = new QToolButton();


    authorLbl->setFixedHeight( 32 );
    authorLbl->setFixedWidth( 48 );
    authorLbl->setAlignment( Qt::AlignCenter );
    authorLbl->setPixmap( QIcon( ":/icons/author.png" ).pixmap( 16 ) );
    authorLbl->setStyleSheet( lblQss );

    authorLE->setFixedHeight( 32 );
    authorLE->setPlaceholderText( "Author1 OR Author2 AND Author3" );
    authorLE->setStyleSheet( leQss );

    textLbl->setFixedHeight( 32 );
    textLbl->setFixedWidth( 48 );
    textLbl->setAlignment( Qt::AlignCenter );
    textLbl->setPixmap( QIcon( ":/icons/text.png" ).pixmap( 16 ) );
    textLbl->setStyleSheet( lblQss );

    textLE->setFixedHeight( 32 );
    textLE->setPlaceholderText( "text1 OR text2 AND text3 NOT text4" );
    textLE->setStyleSheet( leQss );

    dateLbl->setFixedHeight( 32 );
    dateLbl->setFixedWidth( 48 );
    dateLbl->setAlignment( Qt::AlignCenter );
    dateLbl->setPixmap( QIcon( ":/icons/date.png" ).pixmap( 16 ) );
    dateLbl->setStyleSheet( lblQss );

    fromDE->setFixedHeight( 32 );
    fromDE->setFixedWidth( 150 );
    fromDE->setStyleSheet( deQss );

    toLbl->setFixedHeight( 32 );
    toLbl->setBuddy( toDE );

    toDE->setFixedHeight( 32 );
    toDE->setFixedWidth( 150 );
    toDE->setStyleSheet( deQss );

    separator->setFixedSize( 1, 32 );
    separator->setStyleSheet( sepQss );

    clearBtn->setFixedHeight( 32 );
    clearBtn->setFixedWidth( 40 );
    clearBtn->setIcon( QIcon( ":/icons/reset.png" ) );
    clearBtn->setShortcut( tr( "Ctrl+Shift+F" ) );
    clearBtn->setStyleSheet( btnQss );

    addPaperBtn->setFixedHeight( 32 );
    addPaperBtn->setFixedWidth( 40 );
    addPaperBtn->setIcon( QIcon( ":/icons/list-add.png" ) );
    addPaperBtn->setStyleSheet( btnQss );

    getPaperBtn->setFixedHeight( 32 );
    getPaperBtn->setFixedWidth( 40 );
    getPaperBtn->setIcon( QIcon( ":/icons/download.png" ) );
    getPaperBtn->setShortcut( tr( "Ctrl+Shift+N" ) );
    getPaperBtn->setStyleSheet( btnQss );

    QHBoxLayout *lyt = new QHBoxLayout();

    lyt->setContentsMargins( QMargins( 5, 0, 5, 0 ) );
    lyt->setSpacing( 5 );

    lyt->addWidget( authorLbl );
    lyt->addWidget( authorLE );

    lyt->addWidget( textLbl );
    lyt->addWidget( textLE );

    lyt->addWidget( dateLbl );
    lyt->addWidget( fromDE );

    lyt->addWidget( toLbl );
    lyt->addWidget( toDE );

    lyt->addWidget( separator );

    lyt->addWidget( clearBtn );

    lyt->addStretch();

    lyt->addWidget( addPaperBtn );
    lyt->addWidget( getPaperBtn );

    setLayout( lyt );

    setContentsMargins( QMargins() );
    setFixedHeight( 32 );
}


void SearchBar::setupConnections() {
    /** Create a list of filters */
    connect(
        authorLE, &QLineEdit::textEdited, [ = ] ( QString ) {
            if ( not searchTimer->isActive() ) {
                searchTimer->start( 100, Qt::PreciseTimer, this );
            }
        }
    );

    /** Reset the filters */
    connect(
        clearBtn, &QToolButton::clicked, [ = ] () {
            authorLE->clear();
            textLE->clear();

            emit resetFilters();
        }
    );

    /** Add an already downloaded paper */
    connect( addPaperBtn, &QToolButton::clicked, this, &SearchBar::addPaper );

    /** Show dialog when @getPaperBtn is pressed */
    connect( getPaperBtn, &QToolButton::clicked, this, &SearchBar::initiateDownload );
}


void SearchBar::initiateDownload() {
    bool    ok;
    QString doi = QInputDialog::getText(
        this,
        "Papyrus | Download Paper",
        "Enter the DOI of the paper (for example 10.1088/1361-648X/ab4e6f):",
        QLineEdit::Normal,
        "",
        &ok
    );

    if ( ok ) {
        doiSearch->requestPaper( doi );
    }
}


void SearchBar::timerEvent( QTimerEvent *tEvent ) {
    if ( tEvent->timerId() == searchTimer->timerId() ) {
        searchTimer->stop();

        QVariantList filters;

        filters << authorLE->text().split( ", ", Qt::SkipEmptyParts );
        filters << QStringList();
        filters << textLE->text().split( ", ", Qt::SkipEmptyParts );
        filters << QStringList();
        filters << QDate();          // From Month
        filters << QDate();          // From Year
        filters << QDate();          // To Month
        filters << QDate();          // To Year

        emit filterPapers( filters, false );

        return;
    }

    QWidget::timerEvent( tEvent );
}
