/**
 * This file is a part of Papyrus.
 * A Research Paper Manager
 * Copyright 2018-2022 Marcus Britanicus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, vsit http://www.gnu.org/licenses/.
 **/

#pragma once

#include <QString>

static QString leQss = QString(
    "QLineEdit {"
    "   border: 1px solid palette(dark);"
    "   border-radius: 16px;"
    "   padding-left: 10px;"
    "   padding-right: 10px;"
    "}"
    "QLineEdit:focus {"
    "   border: 1px solid palette(highlight);"
    "}"
);

static QString lblQss = QString(
    "QLabel {"
    "   border: 1px solid palette(dark);"
    "   border-radius: 16px;"
    "}"
);

static QString btnQss = QString(
    "QToolButton {"
    "   border: 1px solid palette(dark);"
    "   border-radius: 16px;"
    "}"
    "QToolButton:hover {"
    "   border: 1px solid palette(light);"
    "}"
    "QToolButton:focus {"
    "   border: 1px solid palette(highlight);"
    "}"
    "QToolButton:pressed {"
    "   border: 1px solid palette(highlight);"
    "   margin-left: 1px;"
    "   margin-top: 1px;"
    "}"
);


static QString deQss = QString(
    "QDateTimeEdit {"
    "   border: 1px solid palette(dark);"
    "   border-radius: 16px;"
    "   padding-left: 10px;"
    "   padding-right: 10px;"
    "}"
    "QDateTimeEdit:focus {"
    "   border: 1px solid palette(highlight);"
    "}"
    "QDateTimeEdit::drop-down {"
    "   border: none;"
    "}"
    "QDateTimeEdit::down-arrow {"
    "   image: url(noimg);"
    "   border-width: url(noimg);"
    "}"
);


static QString sepQss = QString(
    "QWidget {"
    "    background-color: palette(light);"
    "    min-width: 1px;"
    "    max-width: 1px;"
    "}"
);
