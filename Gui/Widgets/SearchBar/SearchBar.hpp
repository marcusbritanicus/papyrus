/**
 * This file is a part of Papyrus.
 * A Research Paper Manager
 * Copyright 2018-2022 Marcus Britanicus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, vsit http://www.gnu.org/licenses/.
 **/

#pragma once

#include "Global.hpp"
#include "DateEdit.hpp"

class SearchBar : public QWidget {
    Q_OBJECT;

    public:
        SearchBar( QWidget *parent );

    private:
        void createUI();
        void setupConnections();

        QLabel *authorLbl;
        QLineEdit *authorLE;

        QLabel *textLbl;
        QLineEdit *textLE;

        QLabel *dateLbl, *toLbl;
        DateEdit *fromDE;
        DateEdit *toDE;

        QWidget *separator;

        QToolButton *clearBtn;

        QToolButton *addPaperBtn;
        QToolButton *getPaperBtn;

        QBasicTimer *searchTimer;

        void initiateDownload();

    protected:
        void timerEvent( QTimerEvent * );

    Q_SIGNALS:
        void filterPapers( QVariantList, bool );
        void resetFilters();

        void addPaper();
};
