/**
 * This file is a part of Papyrus.
 * A Research Paper Manager
 * Copyright 2018-2022 Marcus Britanicus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, vsit http://www.gnu.org/licenses/.
 **/

#include "RichLineEdit.hpp"

RichLineEdit::RichLineEdit( QWidget *parent ) : QTextEdit( parent ) {
    /* Font */
    setFont( QFont( "LM Sans 8", 11 ) );

    /* Disable readOnly */
    setReadOnly( false );

    /* Enable rich text */
    setAcceptRichText( true );

    /* No text/word wrapping */
    setWordWrapMode( QTextOption::NoWrap );
    setLineWrapMode( QTextEdit::NoWrap );

    /* Text Alignment */
    setAlignment( Qt::AlignLeft );

    /* Tab changes focus */
    setTabChangesFocus( true );

    /* Focus Policy */
    setFocusPolicy( Qt::StrongFocus );

    /* No Horizontal ScrollBar */
    setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
    horizontalScrollBar()->hide();

    /* No Vertical Scrollbar */
    setVerticalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
    verticalScrollBar()->hide();

    /* Size Policy */
    setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed, QSizePolicy::LineEdit ) );

    /* Margins */
    setViewportMargins( 0, 0, 0, 0 );
    setContentsMargins( 2, 2, 2, 2 );
    document()->setDocumentMargin( 2 );

    /* Action: Bold */
    QAction *boldAct = new QAction( "Bold", this );

    boldAct->setShortcut( tr( "Ctrl+B" ) );
    boldAct->setShortcutContext( Qt::WidgetShortcut );
    connect( boldAct, SIGNAL(triggered()), SLOT(embolden()) );
    addAction( boldAct );

    /* Action: Italic */
    QAction *italicAct = new QAction( "Italic", this );

    italicAct->setShortcut( tr( "Ctrl+I" ) );
    italicAct->setShortcutContext( Qt::WidgetShortcut );
    connect( italicAct, SIGNAL(triggered()), SLOT(italicize()) );
    addAction( italicAct );

    /* Action: Underline */
    QAction *underlineAct = new QAction( "Underline", this );

    underlineAct->setShortcut( tr( "Ctrl+U" ) );
    underlineAct->setShortcutContext( Qt::WidgetShortcut );
    connect( underlineAct, SIGNAL(triggered()), SLOT(underline()) );
    addAction( underlineAct );

    /* Action: Overline */
    QAction *overlineAct = new QAction( "Overline", this );

    overlineAct->setShortcut( tr( "Ctrl+Shift+U" ) );
    overlineAct->setShortcutContext( Qt::WidgetShortcut );
    connect( overlineAct, SIGNAL(triggered()), SLOT(overline()) );
    addAction( overlineAct );

    /* Action: SuperScript */
    QAction *superScriptAct = new QAction( "SuperScript", this );

    superScriptAct->setShortcut( tr( "Alt+Shift+Up" ) );
    superScriptAct->setShortcutContext( Qt::WidgetShortcut );
    connect( superScriptAct, SIGNAL(triggered()), SLOT(superScript()) );
    addAction( superScriptAct );

    /* Action: SubScript */
    QAction *subScriptAct = new QAction( "SubScript", this );

    subScriptAct->setShortcut( tr( "Alt+Shift+Down" ) );
    subScriptAct->setShortcutContext( Qt::WidgetShortcut );
    connect( subScriptAct, SIGNAL(triggered()), SLOT(subScript()) );
    addAction( subScriptAct );

    /* Action: Select All */
    QAction *selectAllAct = new QAction( "Select All", this );

    selectAllAct->setShortcut( tr( "Ctrl+A" ) );
    selectAllAct->setShortcutContext( Qt::WidgetShortcut );
    connect( selectAllAct, SIGNAL(triggered()), SLOT(selectAll()) );
    addAction( selectAllAct );

    /* Action: Deselect All */
    QAction *deselectAllAct = new QAction( "Deselect All", this );

    deselectAllAct->setShortcut( tr( "Ctrl+Shift+A" ) );
    deselectAllAct->setShortcutContext( Qt::WidgetShortcut );
    connect( deselectAllAct, SIGNAL(triggered()), SLOT(deselectAll()) );
    addAction( deselectAllAct );

    /* Action: eng2Greek */
    QAction *eng2GreekAct = new QAction( "English2Greek", this );

    eng2GreekAct->setShortcut( tr( "Ctrl+G" ) );
    eng2GreekAct->setShortcutContext( Qt::WidgetShortcut );
    connect( eng2GreekAct, SIGNAL(triggered()), SLOT(englishToGreek()) );
    addAction( eng2GreekAct );

    /* Action: CodeToSymbol */
    QAction *codeToSymbAct = new QAction( "Code to Symbol", this );

    codeToSymbAct->setShortcut( tr( "Ctrl+Shift+S" ) );
    codeToSymbAct->setShortcutContext( Qt::WidgetShortcut );
    connect( codeToSymbAct, SIGNAL(triggered()), SLOT(codeToSymbols()) );
    addAction( codeToSymbAct );

    /* Paste!!! */
}


QSize RichLineEdit::sizeHint() const {
    ensurePolished();
    QFontMetrics fm = fontMetrics();

    QMargins contMargins = contentsMargins();

    int h = fm.height() + 2 * fm.leading() + contMargins.top() + contMargins.bottom();
    int w = fm.maxWidth() * 20 + contMargins.top() + contMargins.bottom();

    QStyleOptionFrame opt;

    opt.initFrom( this );

    QSize size = style()->sizeFromContents( QStyle::CT_LineEdit, &opt, QSize( w, h ), this );

    h = qMax( QLineEdit().sizeHint().height(), size.height() );

    return QSize( size.width(), h );
}


QSize RichLineEdit::minimumSizeHint() const {
    return sizeHint();
}


QString RichLineEdit::text() {
    QStringList refTokens;

    refTokens << "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">";
    refTokens << "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">";
    refTokens << "p, li { white-space: pre-wrap; }";
    refTokens << "</style></head><body style=\" font-family:'LM Sans 8'; font-size:11pt; font-weight:400; font-style:normal;\">";
    refTokens << "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">";
    refTokens << "</p></body></html>";

    QString html = toHtml().replace( "\n", "" ).replace( "\r", "" );

    Q_FOREACH ( QString token, refTokens ) {
        html.replace( token, "" );
    }

    return html;
}


void RichLineEdit::setText( QString txt ) {
    QString cleanText = txt.replace(
        QRegularExpression(
            "<[ ]*br[ /]*>",
            QRegularExpression::CaseInsensitiveOption | QRegularExpression::InvertedGreedinessOption
        ),
        ""
    ).replace( "\n", "" ).replace( "\r", "" );

    setHtml( cleanText );
}


void RichLineEdit::paste() {
    insertFromMimeData( QApplication::clipboard()->mimeData() );
}


void RichLineEdit::embolden() {
    QTextCursor cursor = textCursor();

    if ( not cursor.hasSelection() ) {
        cursor.select( QTextCursor::WordUnderCursor );
    }

    QTextCharFormat fmt = cursor.charFormat();

    fmt.setFontWeight( fmt.fontWeight() > QFont::Normal ? QFont::Bold : QFont::Normal );
    cursor.mergeCharFormat( fmt );
    mergeCurrentCharFormat( fmt );
}


void RichLineEdit::italicize() {
    QTextCursor cursor = textCursor();

    if ( not cursor.hasSelection() ) {
        cursor.select( QTextCursor::WordUnderCursor );
    }

    QTextCharFormat fmt = cursor.charFormat();

    fmt.setFontItalic( fmt.fontItalic() ? false : true );
    cursor.mergeCharFormat( fmt );
    mergeCurrentCharFormat( fmt );
}


void RichLineEdit::underline() {
    QTextCursor cursor = textCursor();

    if ( not cursor.hasSelection() ) {
        cursor.select( QTextCursor::WordUnderCursor );
    }

    QTextCharFormat fmt = cursor.charFormat();

    fmt.setFontUnderline( fmt.fontUnderline() ? false : true );
    cursor.mergeCharFormat( fmt );
    mergeCurrentCharFormat( fmt );
}


void RichLineEdit::overline() {
    QTextCursor cursor = textCursor();

    if ( not cursor.hasSelection() ) {
        cursor.select( QTextCursor::WordUnderCursor );
    }

    QTextCharFormat fmt = cursor.charFormat();

    fmt.setFontOverline( fmt.fontOverline() ? false : true );
    cursor.mergeCharFormat( fmt );
    mergeCurrentCharFormat( fmt );
}


void RichLineEdit::superScript() {
    QTextCursor cursor = textCursor();

    if ( not cursor.hasSelection() ) {
        cursor.select( QTextCursor::WordUnderCursor );
    }

    QTextCharFormat fmt = cursor.charFormat();

    fmt.setVerticalAlignment(
        fmt.verticalAlignment() == QTextCharFormat::AlignSuperScript ? QTextCharFormat::AlignNormal : QTextCharFormat::AlignSuperScript
    );
    cursor.mergeCharFormat( fmt );
    mergeCurrentCharFormat( fmt );
}


void RichLineEdit::subScript() {
    QTextCursor cursor = textCursor();

    if ( not cursor.hasSelection() ) {
        cursor.select( QTextCursor::WordUnderCursor );
    }

    QTextCharFormat fmt = cursor.charFormat();

    fmt.setVerticalAlignment(
        fmt.verticalAlignment() == QTextCharFormat::AlignSubScript ? QTextCharFormat::AlignNormal : QTextCharFormat::AlignSubScript
    );
    cursor.mergeCharFormat( fmt );
    mergeCurrentCharFormat( fmt );
}


void RichLineEdit::selectAll() {
    QTextCursor cursor = textCursor();

    cursor.select( QTextCursor::Document );
}


void RichLineEdit::deselectAll() {
    QTextCursor cursor = textCursor();

    cursor.clearSelection();
}


void RichLineEdit::englishToGreek() {
    QTextCursor cursor = textCursor();

    if ( not cursor.hasSelection() ) {
        return;
    }

    static QMap<QString, QString> alphabetChart;

    alphabetChart[ "a" ] = QString( "&alpha;" );
    alphabetChart[ "b" ] = QString( "&beta;" );
    alphabetChart[ "c" ] = QString( "&chi;" );
    alphabetChart[ "d" ] = QString( "&delta;" );
    alphabetChart[ "e" ] = QString( "&epsilon;" );
    alphabetChart[ "f" ] = QString( "&phi;" );
    alphabetChart[ "g" ] = QString( "&gamma;" );
    alphabetChart[ "h" ] = QString( "&eta;" );
    alphabetChart[ "i" ] = QString( "&iota;" );
    // alphabetChart[ "j" ] = QString( "j" )			Does not exist
    alphabetChart[ "k" ] = QString( "&kappa;" );
    alphabetChart[ "l" ] = QString( "&lambda;" );
    alphabetChart[ "m" ] = QString( "&mu;" );
    alphabetChart[ "n" ] = QString( "&nu;" );
    alphabetChart[ "o" ] = QString( "&omicron;" );
    alphabetChart[ "p" ] = QString( "&pi;" );
    alphabetChart[ "q" ] = QString( "&theta;" );
    alphabetChart[ "r" ] = QString( "&rho;" );
    alphabetChart[ "s" ] = QString( "&sigma;" );
    alphabetChart[ "t" ] = QString( "&tau;" );
    alphabetChart[ "u" ] = QString( "&upsilon;" );
    // alphabetChart[ "v" ] = QString( "v" )			Does not exist
    alphabetChart[ "w" ] = QString( "&omega;" );
    alphabetChart[ "x" ] = QString( "&xi;" );
    alphabetChart[ "y" ] = QString( "&psi;" );
    alphabetChart[ "z" ] = QString( "&zeta;" );

    alphabetChart[ "A" ] = QString( "&Alpha;" );
    alphabetChart[ "B" ] = QString( "&Beta;" );
    alphabetChart[ "C" ] = QString( "&Chi;" );
    alphabetChart[ "D" ] = QString( "&Delta;" );
    alphabetChart[ "E" ] = QString( "&Epsilon;" );
    alphabetChart[ "F" ] = QString( "&Phi;" );
    alphabetChart[ "G" ] = QString( "&Gamma;" );
    alphabetChart[ "H" ] = QString( "&Eta;" );
    alphabetChart[ "I" ] = QString( "&Iota;" );
    // alphabetChart[ "J" ] = QString( "J" )			Does not exist
    alphabetChart[ "K" ] = QString( "&Kappa;" );
    alphabetChart[ "L" ] = QString( "&Lambda;" );
    alphabetChart[ "M" ] = QString( "&Mu;" );
    alphabetChart[ "N" ] = QString( "&Nu;" );
    alphabetChart[ "O" ] = QString( "&Omicron;" );
    alphabetChart[ "P" ] = QString( "&Pi;" );
    alphabetChart[ "Q" ] = QString( "&Theta;" );
    alphabetChart[ "R" ] = QString( "&Rho;" );
    alphabetChart[ "S" ] = QString( "&Sigma;" );
    alphabetChart[ "T" ] = QString( "&Tau;" );
    alphabetChart[ "U" ] = QString( "&Upsilon;" );
    // alphabetChart[ "V" ] = QString( "V" )			Does not exist
    alphabetChart[ "W" ] = QString( "&Omega;" );
    alphabetChart[ "X" ] = QString( "&Xi;" );
    alphabetChart[ "Y" ] = QString( "&Psi" );
    alphabetChart[ "Z" ] = QString( "&Zeta;" );

    QString sel = cursor.selectedText();
    QString newText;

    Q_FOREACH ( QChar ch, sel ) {
        newText += alphabetChart.value( QString( ch ), QString( ch ) );
    }

    cursor.insertHtml( newText );
}


void RichLineEdit::codeToSymbols() {
    QTextCursor cursor = textCursor();

    if ( not cursor.hasSelection() ) {
        return;
    }

    QSettings symbolsChart( "/usr/share/papyrus/SymbolsChart.conf", QSettings::NativeFormat );

    QString sel     = cursor.selectedText();
    QString newText = symbolsChart.value( sel, sel ).toString();

    cursor.insertHtml( newText );
}


void RichLineEdit::keyPressEvent( QKeyEvent *kEvent ) {
    QList<int> retKeys;

    retKeys << Qt::Key_Enter << Qt::Key_Return;

    if ( retKeys.contains( kEvent->key() ) ) {
        emit editingFinished();
        emit returnPressed();
    }

    else {
        QTextEdit::keyPressEvent( kEvent );
    }

    kEvent->accept();
}


void RichLineEdit::keyReleaseEvent( QKeyEvent *kEvent ) {
    QList<int> retKeys;

    retKeys << Qt::Key_Enter << Qt::Key_Return;

    if ( not retKeys.contains( kEvent->key() ) ) {
        QTextEdit::keyReleaseEvent( kEvent );
    }

    kEvent->accept();
}


void RichLineEdit::insertFromMimeData( const QMimeData *source ) {
    if ( not source ) {
        return;
    }


    QString txt, fragment;

    if ( source->hasFormat( "application/x-qrichtext" ) ) {
        txt = QString::fromUtf8( source->data( "application/x-qrichtext" ) );
    }

    else if ( source->hasHtml() ) {
        txt = source->html();
    }

    else if ( source->hasText() ) {
        txt = source->text();
    }

    QRegularExpression reHead(
        "<head.*head>",
        QRegularExpression::CaseInsensitiveOption | QRegularExpression::InvertedGreedinessOption
    );
    QRegularExpression reBlock(
        QRegularExpression(
            "<[//]{0,1}(div|p|h1|h2|h3|h4|h5|h6|br|hr|body|html|head|!doctype)[^><]*>",
            QRegularExpression::CaseInsensitiveOption | QRegularExpression::InvertedGreedinessOption
        )
    );

    fragment = txt.replace( reHead, "" ).replace( reBlock, "" );

    textCursor().insertHtml( fragment );

    ensureCursorVisible();
}
