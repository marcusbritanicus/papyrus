/**
 * This file is a part of Papyrus.
 * A Research Paper Manager
 * Copyright 2018-2022 Marcus Britanicus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, vsit http://www.gnu.org/licenses/.
 **/

#pragma once

#include "Global.hpp"

class RichLineEdit : public QTextEdit {
    Q_OBJECT

    public:
        RichLineEdit( QWidget *parent = 0 );

        inline bool isModified() {
            return document()->isModified();
        }

        QSize sizeHint() const;
        QSize minimumSizeHint() const;

        QString text();
        void setText( QString );

    public Q_SLOTS:
        void paste();

    private Q_SLOTS:
        void embolden();
        void italicize();
        void underline();
        void overline();

        void superScript();
        void subScript();

        void selectAll();
        void deselectAll();

        void englishToGreek();
        void codeToSymbols();

    protected:
        void keyPressEvent( QKeyEvent *kEvent );
        void keyReleaseEvent( QKeyEvent *kEvent );
        void insertFromMimeData( const QMimeData *source );

    Q_SIGNALS:
        void editingFinished();
        void returnPressed();
};
