/**
 * This file is a part of Papyrus.
 * A Research Paper Manager
 * Copyright 2018-2022 Marcus Britanicus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, vsit http://www.gnu.org/licenses/.
 **/

#pragma once

#include "Global.hpp"
#include "PaperInfo.hpp"

class Paper {
    public:
        /* Invalid Paper */
        Paper();

        /* Init Paper from @path */
        Paper( QString hash, Paper *parent = 0 );

        int childCount();
        void clearChildren();

        void addChild( QString );
        void addChild( Paper * );
        void removeChild( Paper * );

        Paper * child( int );
        QList<Paper *> children();

        Paper *parent();
        int row();

        /* Path */
        QString path();

        /* Paper Title */
        QString title();

        /* Authors */
        QStringList authors();

        /* Date of publication */
        QDate date();

        /* Tags */
        QStringList tags();

        /* PaperInfo */
        PaperInfo paperInfo();

    private:
        QList<Paper *> childNodes;
        Paper *parentNode;
        PaperInfo pInfo;

        QString mPdfPath;
        QString mTitle;
        QStringList mAuthors;
        QDate mDate;
        QStringList mTags;
};
