/**
 * This file is a part of Papyrus.
 * A Research Paper Manager
 * Copyright 2018-2022 Marcus Britanicus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, vsit http://www.gnu.org/licenses/.
 **/

#include "Paper.hpp"

Paper::Paper() {
    parentNode = 0;
}


Paper::Paper( QString hash, Paper *parent ) {
    pInfo      = PaperInfo( hash );
    parentNode = parent;
}


void Paper::addChild( QString hash ) {
    childNodes << new Paper( hash, this );
}


void Paper::addChild( Paper *node ) {
    childNodes << node;
}


void Paper::removeChild( Paper *node ) {
    delete childNodes.takeAt( node->row() );
}


Paper * Paper::child( int row ) {
    return childNodes.at( row );
}


QList<Paper *> Paper::children() {
    return childNodes;
}


int Paper::childCount() {
    return childNodes.count();
}


void Paper::clearChildren() {
    qDeleteAll( childNodes );
    childNodes.clear();
}


Paper * Paper::parent() {
    return parentNode;
}


int Paper::row() {
    if ( parentNode ) {
        return parentNode->childNodes.indexOf( this );
    }

    return 0;
}


QString Paper::path() {
    return pInfo.pdfPath();
}


QString Paper::title() {
    return pInfo.title();
}


QStringList Paper::authors() {
    return pInfo.authors();
}


QDate Paper::date() {
    return pInfo.date();
}


QStringList Paper::tags() {
    return pInfo.tags();
}


PaperInfo Paper::paperInfo() {
    return pInfo;
}
