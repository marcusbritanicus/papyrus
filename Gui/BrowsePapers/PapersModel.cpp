/**
 * This file is a part of Papyrus.
 * A Research Paper Manager
 * Copyright 2018-2022 Marcus Britanicus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, vsit http://www.gnu.org/licenses/.
 **/

#include "Global.hpp"
#include "PapersModel.hpp"
#include "PaperInfo.hpp"

PapersModel::PapersModel( QObject *parent ) : QAbstractListModel( parent ) {
    rootNode = new Paper();
}


QVariant PapersModel::data( const QModelIndex& idx, int role ) const {
    if ( not idx.isValid() ) {
        return QVariant();
    }

    if ( loading ) {
        return QVariant();
    }

    Paper     *node = static_cast<Paper *>(idx.internalPointer() );
    PaperInfo pInfo = node->paperInfo();

    switch ( role ) {
        case Qt::DisplayRole: {
            return node->title();
        }

        case Qt::ToolTipRole: {
            return QString( "<b>%1</b><br><small>%2</small>" ).arg( node->title() ).arg( node->authors().join( ", " ) );
        }

        case PDFPATH: {
            return node->path();
        }

        case TITLE: {
            return node->title();
        }

        case AUTHORS: {
            return node->authors();
        }

        case DATE: {
            return node->date();
        }

        case TAGS: {
            return node->tags();
        }

        case PAPERINFO: {
            return QVariant::fromValue( pInfo );
        }

        case PLAINTITLE: {
            return pInfo.plainTitle();
        }

        /** Useful for filtering */
        case SEARCH: {
            QString searchInfo;
            searchInfo += pInfo.plainTitle() + "\n";
            searchInfo += pInfo.authors().join( "; " ) + "\n";
            searchInfo += pInfo.tags().join( "; " ) + "\n";
            searchInfo += pInfo.abstract() + "\n";

            return searchInfo;
        }

        default: {
            return QVariant();
        }
    }
}


QModelIndex PapersModel::index( int row, int column, const QModelIndex& parent ) const {
    if ( row < 0 or column < 0 ) {
        return QModelIndex();
    }

    if ( (row >= rootNode->childCount() ) and (column != 0) ) {
        return QModelIndex();
    }

    if ( loading ) {
        return QModelIndex();
    }

    Paper *parentNode;

    if ( not parent.isValid() ) {
        parentNode = rootNode;
    }

    else {
        parentNode = static_cast<Paper *>(parent.internalPointer() );
    }

    Paper *childNode = parentNode->child( row );

    if ( childNode ) {
        return createIndex( row, column, childNode );
    }

    else {
        return QModelIndex();
    }
}


void PapersModel::loadPapers() {
    beginResetModel();

    rootNode->clearChildren();
    papersDB->sync();
    loading = true;
    Q_FOREACH ( QString hash, papersDB->childGroups() ) {
        rootNode->addChild( new Paper( hash, rootNode ) );
        qApp->processEvents();
    }

    endResetModel();
    loading = false;

    /* N papers, 1 location for now */
    emit papersLoaded( rootNode->childCount(), 1 );
}
