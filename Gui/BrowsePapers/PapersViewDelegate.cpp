/**
 * This file is a part of Papyrus.
 * A Research Paper Manager
 * Copyright 2018-2022 Marcus Britanicus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, vsit http://www.gnu.org/licenses/.
 **/

#include "PapersViewDelegate.hpp"

void PapersViewDelegate::paint( QPainter *painter, const QStyleOptionViewItem& option, const QModelIndex& index ) const {
    static QIcon mIcon = QIcon( ":/icons/paper.png" );

    if ( index.column() != 0 ) {
        QItemDelegate::paint( painter, option, index );
    }

    else {
        QRect optionRect( option.rect );
        optionRect.setX( 3 );
        optionRect.setSize( mItemSize );

        QSize iconSize( option.decorationSize );

        static int padding = 3;

        int textWidth  = optionRect.width() - optionRect.height() - 2 * padding;
        int textHeight = optionRect.height() - padding;

        QString title   = index.data( Qt::UserRole + 2 ).toString();
        QString authors = index.data( Qt::UserRole + 3 ).toStringList().join( ", " );

        // Font Mentrics for elided text;
        QFontMetrics fm( QFont( qApp->font().family(), qApp->font().pointSize(), QFont::Black ) );
        authors = fm.elidedText( authors, Qt::ElideRight, textWidth );

        painter->save();

        /* Antialiasing for rounded rect */
        painter->setRenderHint( QPainter::Antialiasing, true );

        /* Selection painter settings */
        painter->setPen( QPen( Qt::NoPen ) );
        QColor highlight( option.palette.color( QPalette::Highlight ) );

        if ( (option.state & QStyle::State_Selected) and (option.state & QStyle::State_MouseOver) ) {
            highlight = highlight.darker( 125 );
        }

        else if ( option.state & QStyle::State_Selected ) {
            highlight.setAlphaF( 1.0 );
        }

        else if ( option.state & QStyle::State_MouseOver ) {
            highlight.setAlphaF( 0.3 );
        }

        else {
            highlight.setAlpha( 0 );
        }

        painter->setBrush( highlight );

        /* Paint Background */
        painter->drawRoundedRect( optionRect, 4, 4 );
        painter->restore();

        painter->save();

        /* Focus Rectangle - In our case focus under line */
        if ( option.state & QStyle::State_HasFocus ) {
            painter->setBrush( Qt::NoBrush );
            QPoint bl = optionRect.bottomLeft() + QPoint( 7, -padding / 2 );
            QPoint br = optionRect.bottomRight() - QPoint( 7, padding / 2 );

            QLinearGradient hLine( bl, br );

            hLine.setColorAt( 0,   Qt::transparent );
            hLine.setColorAt( 0.3, option.palette.color( QPalette::BrightText ) );
            hLine.setColorAt( 0.7, option.palette.color( QPalette::BrightText ) );
            hLine.setColorAt( 1,   Qt::transparent );

            painter->setPen( QPen( QBrush( hLine ), 2 ) );
            painter->drawLine( bl, br );
        }

        painter->restore();

        /* Paint Icon */
        if ( option.state & QStyle::State_Selected ) {
            mIcon.paint( painter, QRect( optionRect.x() + 3, optionRect.y() + 3, 40, 40 ), Qt::AlignCenter, QIcon::Selected );
        }

        else if ( option.state & QStyle::State_MouseOver ) {
            mIcon.paint( painter, QRect( optionRect.x() + 3, optionRect.y() + 3, 40, 40 ), Qt::AlignCenter, QIcon::Active );
        }

        else {
            mIcon.paint( painter, QRect( optionRect.x() + 3, optionRect.y() + 3, 40, 40 ), Qt::AlignCenter, QIcon::Normal );
        }

        painter->save();

        /* Draw Text */
        QString defSS = "* {color: %1;}";

        if ( option.state & QStyle::State_Selected ) {
            defSS = defSS.arg( option.palette.color( QPalette::HighlightedText ).name() );
            painter->setPen( option.palette.color( QPalette::HighlightedText ) );
        }

        else if ( option.state & QStyle::State_MouseOver ) {
            defSS = defSS.arg( option.palette.color( QPalette::Text ).name() );
            painter->setPen( option.palette.color( QPalette::Text ) );
        }

        else {
            defSS = defSS.arg( option.palette.color( QPalette::Text ).name() );
            painter->setPen( option.palette.color( QPalette::Text ) );
        }

        QTextOption opt;
        opt.setAlignment( Qt::AlignVCenter | Qt::AlignLeft );
        opt.setWrapMode( QTextOption::WordWrap );

        QTextOption txtOpt( Qt::AlignJustify );
        txtOpt.setWrapMode( QTextOption::WordWrap );

        QTextDocument doc;
        doc.setDefaultStyleSheet( defSS );
        doc.setDefaultTextOption( txtOpt );
        doc.setHtml( "<b>" + title + "</b>" );
        doc.setDefaultTextOption( opt );
        doc.setTextWidth( textWidth );

        painter->translate( optionRect.height() + padding, optionRect.y() );
        doc.drawContents( painter, QRect( 0, 0, textWidth, textHeight / 2 ) );

        QTextLayout titleLyt( title );

        painter->translate( optionRect.height() + padding, optionRect.y() );

        painter->restore();

        painter->save();

        doc.setHtml( "<span>" + authors + "</span>" );
        doc.setTextWidth( textWidth );

        painter->translate( optionRect.height() + padding, optionRect.y() + textHeight / 2 );
        doc.drawContents( painter, QRect( 0, 0, textWidth, textHeight / 2 ) );

        painter->restore();
    }
}


void PapersViewDelegate::setItemSize( QSize newSize ) {
    mItemSize = newSize;
}
