/**
 * This file is a part of Papyrus.
 * A Research Paper Manager
 * Copyright 2018-2022 Marcus Britanicus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, vsit http://www.gnu.org/licenses/.
 **/

#include "Global.hpp"
#include "PapersView.hpp"
#include "PapersModel.hpp"

PapersView::PapersView( QWidget *parent ) : QListView( parent ) {
    // Selection
    setSelectionMode( QAbstractItemView::SingleSelection );
    setSelectionBehavior( QAbstractItemView::SelectRows );

    // No Frame
    setFrameStyle( QFrame::NoFrame );

    // Internal Object Name
    setObjectName( "mainList" );

    // Minimum Size
    setMinimumWidth( 500 );

    // Focus Policy
    setFocusPolicy( Qt::StrongFocus );

    // Mouse tracking
    setMouseTracking( true );

    // ContextMenuPolicy
    setContextMenuPolicy( Qt::CustomContextMenu );
    connect( this, SIGNAL(customContextMenuRequested(const QPoint&)), SLOT(showMenu(const QPoint&)) );

    // No Horizontal ScrollBar
    setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
    horizontalScrollBar()->hide();

    // Always how vertical scrollbar
    setVerticalScrollBarPolicy( Qt::ScrollBarAsNeeded );

    // Icon Mode
    setViewMode( QListView::IconMode );

    // Default Flow
    setFlow( QListView::LeftToRight );

    // Grid Size and Icon Size
    setGridSize( QSize( 500, 54 ) );
    setIconSize( QSize( 40, 40 ) );

    // Item View Delegate
    dlgt = new PapersViewDelegate();
    setItemDelegate( dlgt );

    // Adjust size
    setResizeMode( QListView::Adjust );

    // Hack to get the visual rectangles right
    setFont( QFont( "LM Modern Sans", 30 ) );

    // Unifrm Sizes
    setUniformItemSizes( true );

    // Create actions
    createActions();
}


void PapersView::resizeEvent( QResizeEvent *rEvent ) {
    rEvent->accept();
    setGridSize( QSize( viewport()->width(), 54 ) );
    dlgt->setItemSize( QSize( viewport()->width() - 3, 48 ) );
}


void PapersView::createActions() {
    openAct = new QAction( QString( "&Open Paper" ), this );
    connect( openAct, &QAction::triggered, this, &PapersView::emitOpenPaper );
    openAct->setShortcut( tr( "Ctrl+Return" ) );
    openAct->setShortcutContext( Qt::WidgetShortcut );
    addAction( openAct );

    loadAct = new QAction( QString( "&Load Paper" ), this );
    connect( loadAct, &QAction::triggered, this, &PapersView::emitLoadPaper );
    loadAct->setShortcut( tr( "Return" ) );
    loadAct->setShortcutContext( Qt::WidgetShortcut );
    addAction( loadAct );

    editAct = new QAction( QString( "&Edit Paper" ), this );
    connect( editAct, &QAction::triggered, this, &PapersView::emitEditPaper );
    editAct->setShortcut( tr( "F2" ) );
    editAct->setShortcutContext( Qt::WidgetShortcut );
    addAction( editAct );

    deleteAct = new QAction( QString( "&Delete Paper" ), this );
    connect( deleteAct, &QAction::triggered, this, &PapersView::queryDeletePaper );
    deleteAct->setShortcut( tr( "Delete" ) );
    deleteAct->setShortcutContext( Qt::WidgetShortcut );
    addAction( deleteAct );

    newPaperAct = new QAction( QString( "&Add New Paper" ), this );
    connect( newPaperAct, &QAction::triggered, this, &PapersView::addNewPaper );
    newPaperAct->setShortcut( tr( "Ctrl+N" ) );
    newPaperAct->setShortcutContext( Qt::WidgetShortcut );
    addAction( newPaperAct );
}


void PapersView::showMenu( const QPoint& pos ) {
    /* Init QMenu */
    QMenu menu;

    /* If we have an idex below the mouse */
    if ( indexAt( pos ).isValid() ) {
        setCurrentIndex( indexAt( pos ) );
        menu.addAction( openAct );
        menu.addSeparator();
        menu.addAction( loadAct );
        menu.addAction( editAct );
        menu.addSeparator();
        menu.addAction( deleteAct );
    }

    /* Otherwise */
    else {
        menu.addAction( newPaperAct );
        menu.addSeparator();
        menu.addAction( reloadAct );
    }

    menu.exec( viewport()->mapToGlobal( pos ) );
}


void PapersView::queryDeletePaper() {
    QModelIndex pprIdx = currentIndex();

    if ( not pprIdx.isValid() ) {
        return;
    }

    int ret = QMessageBox::question(
        this,
        "Papyrus | Delete Paper?",
        "Are you sure you want to delete this paper? This action cannot be undone."
    );

    if ( ret == QMessageBox::Yes ) {
        emit deletePaper( pprIdx.data( PapersModel::PDFPATH ).toString() );
    }
}
