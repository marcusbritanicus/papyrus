/**
 * This file is a part of Papyrus.
 * A Research Paper Manager
 * Copyright 2018-2022 Marcus Britanicus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, vsit http://www.gnu.org/licenses/.
 **/

#pragma once

#include "Global.hpp"
#include "PapersViewDelegate.hpp"

class PapersView : public QListView {
    Q_OBJECT

    public:
        PapersView( QWidget *parent = 0 );

    protected:
        void resizeEvent( QResizeEvent * );

    private:
        void createActions();

        // Actions
        QAction *openAct;
        QAction *editAct;
        QAction *loadAct;
        QAction *deleteAct;

        QAction *newPaperAct;
        QAction *reloadAct;

        PapersViewDelegate *dlgt;

    private Q_SLOTS:
        void showMenu( const QPoint& );

        inline void emitOpenPaper() {
            emit openPaper( currentIndex() );
        }

        inline void emitLoadPaper() {
            emit loadPaper( currentIndex() );
        }

        inline void emitEditPaper() {
            emit editPaper( currentIndex() );
        }

        void queryDeletePaper();

    Q_SIGNALS:
        void addNewPaper();
        void openPaper( const QModelIndex& );
        void loadPaper( const QModelIndex& );
        void editPaper( const QModelIndex& );
        void deletePaper( QString );
};
