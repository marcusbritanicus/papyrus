/**
 * This file is a part of Papyrus.
 * A Research Paper Manager
 * Copyright 2018-2022 Marcus Britanicus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, vsit http://www.gnu.org/licenses/.
 **/

#pragma once

#include "Global.hpp"
#include "Paper.hpp"

class PapersModel : public QAbstractListModel {
    Q_OBJECT

    public:
        enum Fields {
            PDFPATH = Qt::UserRole + 1,         // Pdf Path
            TITLE,                              // Paper Title
            AUTHORS,                            // Authors
            DATE,                               // Publication date
            TAGS,                               // Paper Tags
            PAPERINFO,                          // PaperInfo object
            PLAINTITLE,                         // Title as plain text
            SEARCH                              // Search info
        };

        PapersModel( QObject *parent = 0 );

        inline int rowCount( const QModelIndex& ) const {
            /* Let's not count the rows when loading is in progress */
            return (loading ? 0 : rootNode->childCount() );
        }

        inline int columnCount( const QModelIndex& ) const {
            /* We have just one column */
            return 1;
        }

        /* Data about the model */
        QVariant data( const QModelIndex& idx, int role = Qt::DisplayRole ) const;

        /* QModelIndex for (row, col) */
        QModelIndex index( int row = 0, int column = 0, const QModelIndex& parent = QModelIndex() ) const;

        /* QModelIndex for rootNode */
        inline QModelIndex rootIndex() const {
            return createIndex( 0, 0, rootNode );
        }

        inline Qt::ItemFlags flags( const QModelIndex ) const {
            return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
        }

    private:
        /* Load the Papers from Database */
        void loadPapers();

        /* Root Paper */
        Paper *rootNode;

        /** Current state */
        mutable bool loading = false;

    public Q_SLOTS:
        /* Reload the model */
        inline void reload() {
            loadPapers();
        }

    Q_SIGNALS:
        void papersLoading();

        /* N papers loaded from M locations */
        void papersLoaded( long papers, int locations );
};
