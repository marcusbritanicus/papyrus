/**
 * This file is a part of Papyrus.
 * A Research Paper Manager
 * Copyright 2018-2022 Marcus Britanicus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, vsit http://www.gnu.org/licenses/.
 **/

#pragma once

#include "Global.hpp"
#include "PaperInfo.hpp"
#include <QDocumentView.hpp>

class PdfPeek : public QMainWindow {
    Q_OBJECT

    public:
        PdfPeek( QString hash );

    private:
        void createGui();
        void setWindowProperties();
        void setupConnections();

        QLabel *titleLbl;
        QDocumentView *pdfView;

        QPushButton *notesBtn;
        QPushButton *openExtBtn;
        QPushButton *printBtn;

        PaperInfo *pInfo;

        int mZoomFactor = -1;

    public Q_SLOTS:
        void showMaximized();

    private Q_SLOTS:
        void showNotes();
        void openPaper();

        void printPaper();

        void zoomIn();
        void zoomOut();

    protected:
        void resizeEvent( QResizeEvent *rEvent );
};
