/**
 * This file is a part of Papyrus.
 * A Research Paper Manager
 * Copyright 2018-2022 Marcus Britanicus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, vsit http://www.gnu.org/licenses/.
 **/

#include "PdfPeek.hpp"
#include <PopplerDocument.hpp>

PdfPeek::PdfPeek( QString hash ) : QMainWindow() {
    pInfo = new PaperInfo( hash );

    createGui();
    setWindowProperties();
    setupConnections();
}


void PdfPeek::createGui() {
    /* Title Label */
    titleLbl = new QLabel( this );
    titleLbl->setText( QString( "<big><b>%1</b></big><br>%2" ).arg( pInfo->title() ).arg( pInfo->authors().join( ", " ) ) );
    titleLbl->setAlignment( Qt::AlignCenter );
    titleLbl->setWordWrap( true );
    titleLbl->setTextFormat( Qt::RichText );
    titleLbl->setMaximumHeight( 64 );

    /* PdfView */
    pdfView = new QDocumentView( this );
    pdfView->setPageLayout( QDocumentView::SinglePage );
    pdfView->setLayoutContinuous( true );
    pdfView->setZoomMode( QDocumentView::FitToWidth );

    PopplerDocument *doc = new PopplerDocument( pInfo->pdfPath() );

    doc->load();
    pdfView->setDocument( doc );

    /* Open External Button */
    printBtn = new QPushButton( QIcon::fromTheme( "document-print" ), "&Print", this );
    // printBtn->setDisabled( true );

    /* Notes Button */
    notesBtn = new QPushButton( QIcon( ":/icons/wordy.png" ), "Show &Notes", this );

    /* Open External Button */
    openExtBtn = new QPushButton( QIcon( ":/icons/maximize.png" ), "&Open in External", this );

    QHBoxLayout *btnLyt = new QHBoxLayout();

    btnLyt->addWidget( printBtn );
    btnLyt->addStretch();
    btnLyt->addWidget( notesBtn );
    btnLyt->addWidget( openExtBtn );

    /* Base Layout */
    QGridLayout *baseLyt = new QGridLayout();

    baseLyt->addWidget( titleLbl, 0, 0 );
    baseLyt->addWidget( pdfView,  1, 0 );
    baseLyt->addLayout( btnLyt, 2, 0 );

    /* Base Widget */
    QWidget *base = new QWidget( this );

    base->setLayout( baseLyt );

    /* Central Widget */
    setCentralWidget( base );
}


void PdfPeek::setWindowProperties() {
    setWindowTitle( "Papyrus | Preview Paper - " + pInfo->plainTitle() );
    setWindowIcon( QIcon( ":/icons/papyrus.png" ) );

    setMinimumSize( QSize( 800, 600 ) );
}


void PdfPeek::setupConnections() {
    connect( printBtn,   SIGNAL( clicked() ), this, SLOT( printPaper() ) );
    connect( notesBtn,   SIGNAL( clicked() ), this, SLOT( showNotes() ) );
    connect( openExtBtn, SIGNAL(clicked()),   this, SLOT( openPaper() ) );

    QAction *zoomInAct = new QAction( this );

    zoomInAct->setShortcut( QKeySequence( "Ctrl++" ) );
    connect( zoomInAct, SIGNAL(triggered()), this, SLOT(zoomIn()) );
    addAction( zoomInAct );

    QAction *zoomOutAct = new QAction( this );

    zoomOutAct->setShortcut( QKeySequence( "Ctrl+-" ) );
    connect( zoomOutAct, SIGNAL(triggered()), this, SLOT(zoomOut()) );
    addAction( zoomOutAct );
}


void PdfPeek::showMaximized() {
    QMainWindow::showMaximized();
    qApp->processEvents();
}


void PdfPeek::showNotes() {
    QDialog *notesDlg = new QDialog( this );

    notesDlg->setMinimumSize( 640, 480 );

    QTextBrowser *notes = new QTextBrowser( this );

    notes->setHtml( pInfo->notes() );

    QPushButton *closeBtn = new QPushButton( QIcon::fromTheme( "document-close" ), "&Close", this );

    connect( closeBtn, SIGNAL(clicked()), notesDlg, SLOT(close()) );

    QVBoxLayout *dlgLyt = new QVBoxLayout();

    dlgLyt->addWidget( notes );
    dlgLyt->addWidget( closeBtn );

    notesDlg->setLayout( dlgLyt );

    notesDlg->exec();
}


void PdfPeek::printPaper() {
    // PdfPrinter *printer = new PdfPrinter( pInfo->pdfPath(), 0, this );
    // printer->exec();
}


void PdfPeek::zoomIn() {
    if ( mZoomFactor == -1 ) {
        mZoomFactor = pdfView->zoomFactor() * 100;
    }

    if ( mZoomFactor >= 400 ) {
        return;
    }

    /* Increase the zoom by 10 */
    mZoomFactor += 10;
    pdfView->setZoomMode( QDocumentView::CustomZoom );
    pdfView->setZoomFactor( mZoomFactor * 1.0 / 100 );
}


void PdfPeek::zoomOut() {
    if ( mZoomFactor == -1 ) {
        mZoomFactor = pdfView->zoomFactor() * 100;
    }

    if ( mZoomFactor <= 10 ) {
        return;
    }

    /* Increase the zoom by 10 */
    mZoomFactor -= 10;
    pdfView->setZoomMode( QDocumentView::CustomZoom );
    pdfView->setZoomFactor( mZoomFactor * 1.0 / 100 );
}


void PdfPeek::openPaper() {
    setCursor( QCursor( Qt::WaitCursor ) );
    QProcess::startDetached( "xdg-open", QStringList() << pInfo->pdfPath() );
    setCursor( QCursor( Qt::ArrowCursor ) );

    close();
}


void PdfPeek::resizeEvent( QResizeEvent *rEvent ) {
    QMainWindow::resizeEvent( rEvent );
    rEvent->accept();
}
