/**
 * This file is a part of Papyrus.
 * A Research Paper Manager
 * Copyright 2018-2022 Marcus Britanicus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, vsit http://www.gnu.org/licenses/.
 **/

#pragma once

#include "Global.hpp"
#include "PaperInfo.hpp"
#include "RichLineEdit.hpp"
#include "RichTextEdit.hpp"
#include <QDocumentView.hpp>

class AddPaperDialog : public QDialog {
    Q_OBJECT;

    public:

        /**
         * @param data    Pdf file path or hash
         * @param auto    Adding automatically?
         * @param parent  Parent widget
         */
        AddPaperDialog( QString data, bool automatic = false, QWidget *parent = 0 );

    public Q_SLOTS:
        int exec();

        void reject();

        /* Paper Title */
        void setTitle( QString );

        /* Authors */
        void setAuthors( QStringList );

        /* Date of publication */
        void setDate( QDate );

        /* Tags */
        void setTags( QStringList );

        /* Abstract */
        void addAbstract( QString );

        /* Notes */
        void addNotes( QString );

    private:
        void createGui();
        void setWindowProperties();
        void setupConnections();

        QString mHash;
        QString mPdfPath;

        QDocumentView *pdfView;

        RichLineEdit *titleLE;
        QLineEdit *authorLE;
        QLineEdit *tagsLE;
        QDateEdit *dateDE;

        RichTextEdit *abstractTE;
        RichTextEdit *notesTE;

        QPushButton *discardBtn;
        QPushButton *addBtn;
        QPushButton *saveBtn;
        QPushButton *closeBtn;
        QToolButton *reloadBtn;

        PaperInfo *mPaperInfo;

        bool mEditMode   = false;
        bool failedClose = false;

        bool mAuto = false;

    private Q_SLOTS:
        void savePaperInfo();

        inline void addPaper() {
            savePaperInfo();
            accept();
        }

        void reload();

    protected:
        void closeEvent( QCloseEvent *cEvent );
};

class AddPapersManager : public QObject {
    Q_OBJECT;

    public:
        AddPapersManager( QObject *parent = 0 );
        void stop();

    public Q_SLOTS:
        void addPaper( QString pdf );
        void addPapers( QStringList pdfList );

    private:
        QStack<QString> mPdfPathStack;
        bool mStop;
        bool mRunning;

    private Q_SLOTS:
        void run();

    Q_SIGNALS:
        void paperAdded();
};
