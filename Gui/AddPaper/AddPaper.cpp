/**
 * This file is a part of Papyrus.
 * A Research Paper Manager
 * Copyright 2018-2022 Marcus Britanicus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, vsit http://www.gnu.org/licenses/.
 **/

#include "AddPaper.hpp"

#include <PopplerDocument.hpp>

AddPaperDialog::AddPaperDialog( QString data, bool automatic, QWidget *parent ) : QDialog( parent ) {
    mEditMode = data.toLower().endsWith( ".pdf" ) ? false : true;
    mAuto     = automatic;

    createGui();
    setWindowProperties();
    setupConnections();

    /* Add a new Paper */
    if ( data.toLower().endsWith( ".pdf" ) ) {
        mPdfPath = data;
    }

    /* Edit an existing paper */
    else {
        mHash      = QString( data );
        mPaperInfo = new PaperInfo( data );

        setTitle( mPaperInfo->title() );
        setAuthors( mPaperInfo->authors() );
        setDate( mPaperInfo->date() );
        setTags( mPaperInfo->tags() );
        addAbstract( mPaperInfo->abstract() );
        addNotes( mPaperInfo->notes() );

        mPdfPath = mPaperInfo->pdfPath();
    }

    pdfView->setPageLayout( QDocumentView::SinglePage );
    pdfView->setLayoutContinuous( true );
    pdfView->setZoomMode( QDocumentView::FitToWidth );

    setWindowTitle( QString( mEditMode ? "Papyrus | Edit Paper" : "Papyrus | Add a Paper" ) + " | " + QFileInfo( mPdfPath ).fileName() );
}


void AddPaperDialog::createGui() {
    /* Widgets */
    titleLE = new RichLineEdit( this );
    titleLE->setPlaceholderText( "Title of the Paper" );

    authorLE = new QLineEdit( this );
    authorLE->setPlaceholderText( "Authors of the Paper" );

    tagsLE = new QLineEdit( this );
    tagsLE->setPlaceholderText( "Tags to identify the paper" );

    dateDE = new QDateEdit( this );
    dateDE->setDisplayFormat( "MMMM yyyy" );

    abstractTE = new RichTextEdit( this );
    abstractTE->setPlaceholderText( "Abstract of the paper" );

    notesTE = new RichTextEdit( this );
    notesTE->setPlaceholderText( "Yours notes on this paper" );

    QLabel *titleLbl    = new QLabel( "&Title:" );
    QLabel *authorLbl   = new QLabel( "A&uthors:" );
    QLabel *tagsLbl     = new QLabel( "Ta&gs:" );
    QLabel *dateLbl     = new QLabel( "&Date:" );
    QLabel *abstractLbl = new QLabel( "A&bstract:" );
    QLabel *notesLbl    = new QLabel( "N&otes:" );

    titleLbl->setBuddy( titleLE );
    authorLbl->setBuddy( authorLE );
    tagsLbl->setBuddy( tagsLE );
    dateLbl->setBuddy( dateDE );
    abstractLbl->setBuddy( abstractTE );
    notesLbl->setBuddy( notesTE );

    reloadBtn = new QToolButton( this );
    reloadBtn->setIcon( QIcon::fromTheme( "view-refresh" ) );

    pdfView = new QDocumentView( this );
    connect(
        pdfView, &QDocumentView::documentLoadingFailed, [ = ] {
            done( Rejected );
            close();
        }
    );

    QLabel *previewLbl = new QLabel( "&Preview: " + QFileInfo( mPdfPath ).baseName() );

    previewLbl->setBuddy( pdfView );

    if ( not mEditMode ) {
        addBtn = new QPushButton( this );
        addBtn->setText( "&Add" );
        addBtn->setIcon( QIcon( ":/icons/list-add.png" ) );
    }

    else {
        saveBtn = new QPushButton( this );
        saveBtn->setText( "&Save Changes" );
        saveBtn->setShortcut( tr( "Ctrl+S" ) );
        saveBtn->setIcon( QIcon::fromTheme( "document-save" ) );
    }

    /* Buttons */
    discardBtn = new QPushButton( this );
    discardBtn->setText( "Dis&card" );
    discardBtn->setIcon( QIcon( ":/icons/discard.png" ) );

    /* Close Button */
    closeBtn = new QPushButton( this );
    closeBtn->setText( "&Close" );
    closeBtn->setIcon( QIcon::fromTheme( "document-close" ) );

    /* Buttons Layout */
    QHBoxLayout *btnLyt = new QHBoxLayout();

    btnLyt->addWidget( closeBtn );
    btnLyt->addStretch();
    btnLyt->addWidget( discardBtn );
    btnLyt->addWidget( mEditMode ? saveBtn : addBtn );

    /* Edit Layout */
    QGridLayout *editLyt = new QGridLayout();

    editLyt->addWidget( titleLbl,    0, 0 );
    editLyt->addWidget( titleLE,     0, 1 );
    editLyt->addWidget( authorLbl,   1, 0 );
    editLyt->addWidget( authorLE,    1, 1 );
    editLyt->addWidget( tagsLbl,     2, 0 );
    editLyt->addWidget( tagsLE,      2, 1 );
    editLyt->addWidget( abstractLbl, 3, 0 );
    editLyt->addWidget( dateDE,      3, 1, Qt::AlignVCenter | Qt::AlignRight );
    editLyt->addWidget( abstractTE,  4, 0, 1, 2 );
    editLyt->addWidget( notesLbl,    5, 0 );
    editLyt->addWidget( notesTE,     6, 0, 1, 2 );
    editLyt->addLayout( btnLyt, 7, 0, 1, 2 );

    /* Preview Layout */
    QHBoxLayout *preBaseLyt = new QHBoxLayout();

    preBaseLyt->addWidget( previewLbl );
    preBaseLyt->addWidget( reloadBtn );
    QVBoxLayout *previewLyt = new QVBoxLayout();

    previewLyt->addLayout( preBaseLyt );
    previewLyt->addWidget( pdfView );

    /* Base Layout */
    QHBoxLayout *baseLyt = new QHBoxLayout();

    baseLyt->addLayout( editLyt );
    baseLyt->addLayout( previewLyt );

    /* Set Layout */
    setLayout( baseLyt );
}


void AddPaperDialog::setWindowProperties() {
    setWindowTitle( mEditMode ? "Papyrus | Edit Paper" : "Papyrus | Add a Paper" );
    setWindowIcon( QIcon( ":/icons/papyrus.png" ) );

    setWindowModality( Qt::ApplicationModal );
}


void AddPaperDialog::setupConnections() {
    if ( not mEditMode ) {
        connect( addBtn, &QPushButton::clicked, this, &AddPaperDialog::addPaper );
    }

    else {
        connect( saveBtn, &QPushButton::clicked, this, &AddPaperDialog::savePaperInfo );
    }

    connect( reloadBtn,  &QPushButton::clicked, this, &AddPaperDialog::reload );
    connect( discardBtn, &QPushButton::clicked, this, &AddPaperDialog::reject );
    connect( closeBtn,   &QPushButton::clicked, this, &AddPaperDialog::close );
}


void AddPaperDialog::reload() {
    /** Reload the document */
    PopplerDocument *doc = new PopplerDocument( mPdfPath );

    doc->load();

    /** Set the document only if it was successful */
    if ( (doc->status() == QDocument::Ready) and (doc->error() == QDocument::NoError) ) {
        pdfView->setDocument( doc );

        pdfView->setPageLayout( QDocumentView::SinglePage );
        pdfView->setLayoutContinuous( true );
        pdfView->setZoomMode( QDocumentView::FitToWidth );
    }

    /** Don't continue if the loading failed. */
    else {
        QMessageBox::critical(
            this,
            "Papyrus | Failed to Open PDF",
            "Unable to load the PDF file <tt>" + QFileInfo( mPdfPath ).fileName() + "</tt>. "
            "Please ensure that the PDF file is not corrupted."
        );

        return;
    }

    /**
     * Paper was automatically added.
     * So if we're hitting refresh, it means we don't have the required data.
     */
    if ( mAuto ) {
        if ( abstractTE->toPlainText().isEmpty() ) {
            abstractTE->setText( pdfView->document()->pageText( 0 ) + "\n\n" + pdfView->document()->pageText( 1 ) );
        }

        return;
    }

    /** Get the paper info */
    bool    ok;
    QString doi = QInputDialog::getText(
        this,
        "Papyrus | Download Paper Metadata",
        "Enter the DOI of the paper (for example 10.1088/1361-648X/ab4e6f):",
        QLineEdit::Normal,
        "",
        &ok
    );

    if ( ok ) {
        CurlRunner *request = new CurlRunner( doi, 0x01 );
        abstractTE->clear();

        connect(
            request, &CurlRunner::infoReady, [ = ] ( QString, QString info ) mutable {
                QJsonObject jInfo = QJsonDocument::fromJson( info.toUtf8() ).object();
                titleLE->setText( jInfo[ "title" ].toString() );

                /** Save the authors list */
                QStringList authors;

                for ( QJsonValue authV: jInfo[ "authors" ].toArray() ) {
                    authors << authV.toObject()[ "name" ].toString().replace( ".", "" );
                }
                setAuthors( authors );

                /** Abstract might be in HTML format */
                abstractTE->setHtml( jInfo[ "abstract" ].toString() );
            }
        );

        request->run();
    }

    /** Info retrieval failed. We now load the abstract from */
    if ( abstractTE->toPlainText().simplified().isEmpty() ) {
        abstractTE->setText( pdfView->document()->pageText( 0 ) + "\n\n" + pdfView->document()->pageText( 1 ) );
    }
}


int AddPaperDialog::exec() {
    showMaximized();

    reload();

    if ( pdfView->document() == nullptr ) {
        done( QDialog::Rejected );
        return QDialog::Rejected;
    }

    return QDialog::exec();
}


void AddPaperDialog::reject() {
    /* If we press the "Discard" button, then just discard and quit. */
    if ( qobject_cast<QPushButton *>( sender() ) ) {
        done( Rejected );
        return;
    }

    /* We arrived here by hitting "Escape": Make sure user knows there are changes waiting, if any. */

    bool modified = false;

    modified |= titleLE->isModified();
    modified |= authorLE->isModified();
    modified |= tagsLE->isModified();
    // modified |= dateDE->isModified();

    modified |= abstractTE->isModified();
    modified |= notesTE->isModified();

    if ( modified ) {
        int reply = QMessageBox::question(
            this,
            "Papyrus | Paper Info Modfied",
            "Some information about this paper has been modified. Press <tt>Discard</tt> to <b><i>discard</i></b> these changes. "
            "Pressing <tt>Cancel</tt> will retain changes and close this message.",
            QMessageBox::Cancel | QMessageBox::Discard,
            QMessageBox::Cancel
        );

        /* If the user chooses to reject the discard the changes, reject the dialog */
        if ( reply == QMessageBox::Discard ) {
            QDialog::reject();
        }

        /* Otherwise, just return to the dialog UI. */
        else {
            return;
        }
    }

    /* If there was no modification, then reject the dialog */
    QDialog::reject();
}


void AddPaperDialog::setTitle( QString newTitle ) {
    titleLE->setText( newTitle );
}


void AddPaperDialog::setAuthors( QStringList newAuthors ) {
    authorLE->setText( newAuthors.join( ", " ) );
}


void AddPaperDialog::setDate( QDate newDate ) {
    dateDE->setDate( newDate );
}


void AddPaperDialog::setTags( QStringList newTags ) {
    tagsLE->setText( newTags.join( ", " ) );
}


void AddPaperDialog::addAbstract( QString newAbstract ) {
    abstractTE->setHtml( newAbstract );
}


void AddPaperDialog::addNotes( QString newNotes ) {
    notesTE->setHtml( newNotes );
}


void AddPaperDialog::savePaperInfo() {
    /* Adding new paper */
    if ( not mHash.length() ) {
        mPaperInfo = new PaperInfo( mPdfPath );
        mHash      = mPaperInfo->hash();

        QStringList addedPapers = papersDB->value( "AddedPaths" ).toStringList();
        papersDB->setValue( "AddedPaths", addedPapers << mPdfPath );
        papersDB->sync();
    }

    mPaperInfo->setTitle( titleLE->text() );
    mPaperInfo->setAuthors( authorLE->text().split( ", " ) );
    mPaperInfo->setDate( dateDE->date() );
    mPaperInfo->setTags( tagsLE->text().split( ", " ) );
    mPaperInfo->addAbstract( abstractTE->toHtml().replace( "\n", "" ) );
    mPaperInfo->addNotes( notesTE->toHtml().replace( "\n", "" ) );

    titleLE->document()->setModified( false );
    authorLE->setModified( false );
    tagsLE->setModified( false );
    abstractTE->document()->setModified( false );
    notesTE->document()->setModified( false );
}


void AddPaperDialog::closeEvent( QCloseEvent *cEvent ) {
    if ( failedClose ) {
        cEvent->accept();
        return;
    }

    bool modified = false;

    modified |= titleLE->isModified();
    modified |= authorLE->isModified();
    modified |= tagsLE->isModified();

    modified |= abstractTE->isModified();
    modified |= notesTE->isModified();

    if ( modified ) {
        int reply = QMessageBox::question(
            this,
            "Papyrus | Paper Info Modfied",
            "Some information about this paper has been modified. Press <tt>Save</tt> to save the changes and close, "
            "<tt>Discard</tt> to discard these changes and close, and pressing <tt>Cancel</tt> will retain the changes and close this message.",
            QMessageBox::Cancel | QMessageBox::NoButton | QMessageBox::Discard | QMessageBox::Save,
            QMessageBox::Save
        );

        if ( reply == QMessageBox::Save ) {
            savePaperInfo();
            cEvent->accept();
        }

        else if ( reply == QMessageBox::Cancel ) {
            cEvent->ignore();
            return;
        }
    }

    cEvent->accept();
}


AddPapersManager::AddPapersManager( QObject *parent ) : QObject( parent ) {
    mStop    = false;
    mRunning = false;

    mPdfPathStack.clear();
}


void AddPapersManager::stop() {
    mStop = true;
}


void AddPapersManager::addPaper( QString pdf ) {
    /** Should be a .pdf file */
    if ( not pdf.toLower().endsWith( ".pdf" ) ) {
        return;
    }

    /** Check the mimetype using data */
    QMimeType mime = mimeDb.mimeTypeForFile( pdf, QMimeDatabase::MatchContent );

    /** Proceed only if it's a PDF file */
    if ( mime.name() != "application/pdf" ) {
        return;
    }

    /* Add this pdf to the stack only if its not been added before */
    if ( not papersDB->value( "AddedPaths" ).toStringList().contains( pdf ) ) {
        if ( not mPdfPathStack.contains( pdf ) ) {
            mPdfPathStack.push( pdf );
        }
    }

    if ( not mRunning and mPdfPathStack.length() ) {
        QTimer::singleShot( 100, this, &AddPapersManager::run );
    }
}


void AddPapersManager::addPapers( QStringList pdfList ) {
    QStringList addedPapers;

    addedPapers << papersDB->value( "AddedPaths" ).toStringList();

    Q_FOREACH ( QString pdf, pdfList ) {
        /* Add this pdf to the stack only if its not been added before */
        if ( not addedPapers.contains( pdf ) ) {
            mPdfPathStack.push( pdf );
        }
    }

    if ( not mRunning and mPdfPathStack.length() ) {
        QTimer::singleShot( 100, this, &AddPapersManager::run );
    }
}


void AddPapersManager::run() {
    mRunning = true;

    do {
        if ( mStop ) {
            return;
        }

        AddPaperDialog *addPaper = new AddPaperDialog( mPdfPathStack.pop() );
        addPaper->exec();

        if ( addPaper->result() == QDialog::Accepted ) {
            emit paperAdded();
        }
    } while ( mPdfPathStack.length() );

    mRunning = false;
}
