/**
 * This file is a part of Papyrus.
 * A Research Paper Manager
 * Copyright 2018-2022 Marcus Britanicus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, vsit http://www.gnu.org/licenses/.
 **/

#pragma once

#include "Global.hpp"
#include "PapersModel.hpp"
#include "PapersView.hpp"
#include "RichTextEdit.hpp"
#include "AddPaper.hpp"
#include "PdfPeek.hpp"
#include "PaperWatcher.hpp"
#include "SearchBar.hpp"

class UI : public QMainWindow {
    Q_OBJECT;

    public:
        UI();

    private:
        /* UI */
        void createGui();
        void setWindowProperties();
        void setupConnections();
        void setupSystemTrayIcon();

        PapersView *view;
        PapersModel *model;
        QSortFilterProxyModel *filterModel;

        SearchBar *searchBar;
        PaperWatcher *paperWatcher;

        QToolButton *abstractBtn;
        QToolButton *notesBtn;
        QToolButton *openBtn;

        QModelIndex currentPdfIdx;

        QStatusBar *sBar;

        AddPapersManager *addPapersManager;

        void addNewPaper();
        void downloadPaper( QString, QString, QString, QString );
        void deletePaper( QString );
        void openPaper( const QModelIndex& idx = QModelIndex() );
        void previewPaper( const QModelIndex& );
        void editPaper( const QModelIndex& );

        void showPapersLoadInfo( long, int );
        void handleTrayActivation( QSystemTrayIcon::ActivationReason );
};
