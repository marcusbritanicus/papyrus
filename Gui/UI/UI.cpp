/**
 * This file is a part of Papyrus.
 * A Research Paper Manager
 * Copyright 2018-2022 Marcus Britanicus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, vsit http://www.gnu.org/licenses/.
 **/

#include "UI.hpp"

UI::UI() : QMainWindow() {
    setupSystemTrayIcon();

    createGui();
    setWindowProperties();
    setupConnections();

    /* Initiate the AddPapersManager */
    addPapersManager = new AddPapersManager();
    connect( addPapersManager, SIGNAL(paperAdded()), model, SLOT(reload()) );

    /* Start the paper watcher */
    paperWatcher = new PaperWatcher();
    for ( QString pprPath: papersPath ) {
        paperWatcher->addWatch( pprPath );
    }

    connect( paperWatcher, &PaperWatcher::paperAdded,   addPapersManager, &AddPapersManager::addPaper );
    connect( paperWatcher, &PaperWatcher::paperDeleted, this,             &UI::deletePaper );

    paperWatcher->startWatch();

    /* Set the focus on PapersView, and load the model */
    view->setFocus();
    model->reload();
}


void UI::setupSystemTrayIcon() {
    QSystemTrayIcon *trayIcon = new QSystemTrayIcon( QIcon( ":/icons/papyrus.png" ) );

    connect( trayIcon, &QSystemTrayIcon::activated, this, &UI::handleTrayActivation );

    trayIcon->show();
}


void UI::createGui() {
    /* PapersView and PapersModel */
    model = new PapersModel();

    /** Sort Filter model */
    filterModel = new QSortFilterProxyModel();
    filterModel->setSourceModel( model );

    /** Sort by title */
    filterModel->setSortCaseSensitivity( Qt::CaseInsensitive );
    filterModel->setSortRole( PapersModel::PLAINTITLE );

    /** Filter by Info and CaseInsensitive */
    filterModel->setFilterCaseSensitivity( Qt::CaseInsensitive );
    filterModel->setFilterRole( PapersModel::SEARCH );

    view = new PapersView( this );
    view->setModel( filterModel );

    /* Deep Find Widget */
    searchBar = new SearchBar( this );

    sBar = statusBar();
    sBar->setMinimumHeight( 27 );

    /* Base Layout */
    QVBoxLayout *baseLyt = new QVBoxLayout();

    baseLyt->setContentsMargins( QMargins( 0, 5, 0, 0 ) );
    baseLyt->setSpacing( 5 );
    baseLyt->addWidget( searchBar );
    baseLyt->addWidget( view );

    /* Base Widget */
    QWidget *base = new QWidget();

    base->setContentsMargins( QMargins() );
    base->setLayout( baseLyt );

    /* Set the Central Widget */
    setCentralWidget( base );
}


void UI::setWindowProperties() {
    setWindowTitle( "Papyrus | Research Paper Manager" );
    setWindowIcon( QIcon( ":/icons/papyrus.png" ) );

    setMinimumSize( QSize( 800, 600 ) );
}


void UI::setupConnections() {
    connect( model, &PapersModel::papersLoading, filterModel, &QSortFilterProxyModel::invalidate );

    connect(
        model, &PapersModel::papersLoaded, [ = ] () {
            filterModel->invalidate();
            QTimer::singleShot(
                250, [ this ] () {
                    filterModel->sort( Qt::DisplayRole );
                }
            );
        }
    );

    connect( model, &PapersModel::papersLoaded, this, &UI::showPapersLoadInfo );

    connect( view,  &PapersView::activated,     this, &UI::previewPaper );
    connect( view,  &PapersView::addNewPaper,   this, &UI::addNewPaper );
    connect( view,  &PapersView::openPaper,     this, &UI::openPaper );
    connect( view,  &PapersView::loadPaper,     this, &UI::previewPaper );
    connect( view,  &PapersView::editPaper,     this, &UI::editPaper );
    connect( view,  &PapersView::deletePaper,   this, &UI::deletePaper );

    connect(
        searchBar, &SearchBar::filterPapers, [ = ] ( QVariantList filters ) {
            QStringList authors = filters.at( 0 ).toStringList();
            QStringList words   = filters.at( 2 ).toStringList();

            QString rxStr;
            for ( QString elm: authors + words ) {
                rxStr += QString( "(%1)|" ).arg( elm );
            }
            rxStr.chop( 1 );

            filterModel->setFilterRegularExpression( rxStr );
        }
    );

    connect(
        searchBar, &SearchBar::resetFilters, [ = ] () {
            filterModel->setFilterRegularExpression( "" );
            view->setFocus();
        }
    );

    connect( searchBar, &SearchBar::addPaper,          this, &UI::addNewPaper );

    connect( doiSearch, &PaperSearch::paperDownloaded, this, &UI::downloadPaper );
    connect(
        doiSearch, &PaperSearch::searchFailed, [ this ] ( QString doi ) {
            QMessageBox::critical(
                this,
                "Papyrus | Search Failed",
                QString(
                    "Failed to obtain any information or download the PDF for the given DOI."
                    "<p><center><tt>%1</tt></center></p>"
                ).arg( doi )
            );
        }
    );

    /* Reload database*/
    QAction *reloadAct = new QAction( QIcon::fromTheme( "view-refresh" ), "Reload", this );

    connect( reloadAct, &QAction::triggered, model, &PapersModel::reload );
    reloadAct->setShortcuts( QList<QKeySequence>() << tr( "Ctrl+R" ) << tr( "F5" ) );
    addAction( reloadAct );

    /* Quit Papyrus */
    QAction *quitAct = new QAction( QIcon::fromTheme( "application-exit" ), "Quit Papyrus", this );

    connect( quitAct, &QAction::triggered, qApp, &QApplication::quit );
    quitAct->setShortcut( tr( "Ctrl+Shift+Q" ) );
    addAction( quitAct );
}


void UI::addNewPaper() {
    QStringList pdfPaths = QFileDialog::getOpenFileNames(
        this,
        tr( "Papyrus | Add a Paper" ),
        papersPath.at( 0 ),
        tr( "Pdf Document (*.pdf)" )
    );

    addPapersManager->addPapers( pdfPaths );
}


void UI::downloadPaper( QString doi, QString info, QString pdf, QString bbtx ) {
    /** Only half the information is available */
    if ( info.isEmpty() or pdf.isEmpty() ) {
        QMessageBox::information(
            this,
            "Papyrus | Incomplete information",
            "We were unable to retrieve " + QString( info.isEmpty() ? "metadata" : "pdf" ) + " for the article "
            "with DOI <tt>" + doi + "</tt>. This article will not be currently added to your database."
        );

        return;
    }

    QString pdfPath = papersPath.at( 0 ) + "/" + QFileInfo( pdf ).fileName();

    /** Add this path to the database. */
    QStringList addedPapers = papersDB->value( "AddedPaths" ).toStringList();

    papersDB->setValue( "AddedPaths", addedPapers << pdfPath );
    papersDB->sync();

    QJsonObject jInfo  = QJsonDocument::fromJson( info.toUtf8() ).object();
    PaperInfo   *pInfo = new PaperInfo( pdfPath );

    /** Save the title */
    pInfo->setTitle( jInfo[ "title" ].toString() );

    /** Save the authors list */
    QStringList authors;

    for ( QJsonValue authV: jInfo[ "authors" ].toArray() ) {
        authors << authV.toObject()[ "name" ].toString().replace( ".", "" );
    }
    pInfo->setAuthors( authors );

    /** Save a dummy value as date */
    pInfo->setDate( QDate( 1, 1, 2000 ) );      // Dummy Date

    /** Save the retrieved abstract */
    pInfo->addAbstract( jInfo[ "abstract" ].toString() );

    /** Save the bib entry and print it to the terminal */
    if ( not bbtx.isEmpty() ) {
        QString bibFilePath = papSett->value( "BibTexFile" ).toString();

        if ( not bibFilePath.isEmpty() ) {
            QFile bibFile( bibFilePath );

            if ( bibFile.open( QFile::Append ) ) {
                bibFile.write( bbtx.toUtf8() );
                bibFile.write( "\n" );
                bibFile.close();
            }
        }

        qDebug() << "Bib:" << doi.toUtf8().data();
        qDebug() << bbtx.toUtf8().data();
    }

    /** Move the paper to @pdfPath */
    if ( QFile::copy( pdf, pdfPath ) ) {
        QFile::remove( pdf );
    }

    /** Now, open this paper for editing */
    AddPaperDialog *addPaper = new AddPaperDialog( pInfo->hash(), true, this );

    addPaper->exec();

    model->reload();
}


void UI::deletePaper( QString pdfPath ) {
    QStringList addedPapers;

    addedPapers << papersDB->value( "AddedPaths" ).toStringList();
    addedPapers.removeAll( pdfPath );
    addedPapers.removeDuplicates();

    papersDB->setValue( "AddedPaths", addedPapers );

    for ( QString key: papersDB->childGroups() ) {
        if ( papersDB->value( key + "/Path" ).toString() == pdfPath ) {
            papersDB->remove( key );
            break;
        }
    }

    model->reload();
}


void UI::openPaper( const QModelIndex& idx ) {
    setCursor( QCursor( Qt::WaitCursor ) );

    if ( idx.isValid() ) {
        QProcess::startDetached( "xdg-open", QStringList() << idx.data( PapersModel::PDFPATH ).toString() );
    }

    else {
        QProcess::startDetached( "xdg-open", QStringList() << currentPdfIdx.data( PapersModel::PDFPATH ).toString() );
    }

    setCursor( QCursor( Qt::ArrowCursor ) );
}


void UI::editPaper( const QModelIndex& idx ) {
    PaperInfo      pInfo     = idx.data( PapersModel::PAPERINFO ).value<PaperInfo>();
    AddPaperDialog *addPaper = new AddPaperDialog( pInfo.hash(), true, this );

    addPaper->exec();

    model->reload();
}


void UI::previewPaper( const QModelIndex& idx ) {
    PaperInfo pInfo = idx.data( PapersModel::PAPERINFO ).value<PaperInfo>();

    PdfPeek *pdfPeek = new PdfPeek( pInfo.hash() );

    pdfPeek->showMaximized();
}


void UI::handleTrayActivation( QSystemTrayIcon::ActivationReason reason ) {
    switch ( reason ) {
        case QSystemTrayIcon::Context: {
            /* No context menu */
            return;
        }

        case QSystemTrayIcon::DoubleClick:
        case QSystemTrayIcon::Trigger: {
            if ( isVisible() ) {
                hide();
            }

            else {
                showMaximized();
            }

            break;
        }

        case QSystemTrayIcon::MiddleClick: {
            qDebug() << "Good Bye..!";
            qApp->quit();

            break;
        }

        case QSystemTrayIcon::Unknown: {
            qDebug() << "Unknown action!";
            break;
        }
    }
}


void UI::showPapersLoadInfo( long numPapers, int numLocs ) {
    sBar->showMessage( QString( "%1 papers loaded from %2 locations." ).arg( numPapers ).arg( numLocs ), 2000 );
}
