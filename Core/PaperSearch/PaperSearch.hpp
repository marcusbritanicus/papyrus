/**
 * This file is a part of Papyrus.
 * A Research Paper Manager
 * Copyright 2018-2022 Marcus Britanicus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, vsit http://www.gnu.org/licenses/.
 **/

#pragma once

#include <string>
#include <curl/curl.h>

#include <QtCore>

class CurlRunner : public QObject, public QRunnable {
    Q_OBJECT;

    public:
        CurlRunner( QString doi, int what );

        void run();

    private:
        CURL *handle;
        QString mDoi;
        int mWhat;

        void getPaperInfo();
        void fetchPaper();
        void fetchBibTex();

    Q_SIGNALS:
        void infoReady( QString, QString );
        void infoFailed( QString );

        void paperReady( QString, QString );
        void paperFailed( QString );

        void bibReady( QString, QString );
        void bibFailed( QString );
};

class PaperSearch : public QObject {
    Q_OBJECT;

    public:
        PaperSearch( QObject *parent = nullptr );

        void requestPaper( QString doi, int what = 0x07 );

    private:
        QHash<QString, QString> infoHash;
        QHash<QString, QString> pathHash;
        QHash<QString, QString> bbtxHash;

        void emitIfReady( QString doi );

        /**
         * Search what?
         * 0x01 - Info only
         * 0x02 - Pdf only
         * 0x04 - BibTex only
         */
        int searchWhat = 0;

    Q_SIGNALS:

        /**
         * This signal is emitted once the info and the paper search is complete.
         * Does not guarantee availability of any. It's the reciever's responsibility
         * to verify the data.
         *
         * @param doi    DOI of the Paper
         * @param info   Title, Author, Abstract info of the Paper obtained from SemanticScholar
         * @param pdf    Local path to PDF of the paper obtained from http://sci-hub.ee
         * @param bibtex Bibtex entry
         */
        void paperDownloaded( QString doi, QString info, QString pdf, QString bibtex );

        /**
         * This signal is emotted when the info search, pdf search and the bibtex search fails.
         */
        void searchFailed( QString doi );
};
