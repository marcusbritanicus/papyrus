/**
 * This file is a part of Papyrus.
 * A Research Paper Manager
 * Copyright 2018-2022 Marcus Britanicus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, vsit http://www.gnu.org/licenses/.
 **/

#include "PaperSearch.hpp"
#include "Global.hpp"

using QRX = QRegularExpression;

CurlRunner::CurlRunner( QString doi, int what ) {
    mDoi  = doi;
    mWhat = what;
}


void CurlRunner::getPaperInfo() {
    handle = curl_easy_init();

    QString response;

    QString api = "https://api.semanticscholar.org/v1/paper/" + mDoi;

    curl_easy_setopt( handle, CURLOPT_URL, api.toUtf8().data() );
    curl_easy_setopt(
        handle, CURLOPT_WRITEFUNCTION, +[] (char *buffer, size_t size, size_t nmemb, QString * str)->size_t {
            size_t newLength = size * nmemb;
            try {
                str->append( QString::fromUtf8( buffer, newLength ) );
            }catch ( std::bad_alloc& e ) {
                return 0;
            }

            return newLength;
        }
    );

    curl_easy_setopt( handle, CURLOPT_WRITEDATA, &response );
    /** Should not take more than 30s to get the info */
    curl_easy_setopt( handle, CURLOPT_TIMEOUT,   30 );

    /** Perform the request */
    CURLcode ret = curl_easy_perform( handle );

    /** Cleanup the handle */
    curl_easy_cleanup( handle );

    /** Emit the signal only if we recieved some data */
    if ( response.length() or (ret == CURLE_OK) ) {
        /** Curl worked, but DOI was not found. */
        if ( response.contains( "error" ) and response.contains( "not found" ) ) {
            emit infoFailed( mDoi );
        }

        else {
            emit infoReady( mDoi, response );
        }
    }

    /** Otherwise state that we have failed */
    else {
        emit infoFailed( mDoi );
    }
}


void CurlRunner::fetchPaper() {
    handle = curl_easy_init();

    QString response;

    QStringList scihuburls = {};

    QString url = "https://sci-hub.ru/" + mDoi;
    QString pdf;

    curl_easy_setopt( handle, CURLOPT_URL, url.toUtf8().data() );
    curl_easy_setopt(
        handle, CURLOPT_WRITEFUNCTION, +[] (char *buffer, size_t size, size_t nmemb, QString * str)->size_t {
            size_t newLength = size * nmemb;
            try {
                str->append( QString::fromUtf8( buffer, newLength ) );
            }catch ( std::bad_alloc& e ) {
                return 0;
            }

            return newLength;
        }
    );

    curl_easy_setopt( handle, CURLOPT_WRITEDATA,      &response );
    curl_easy_setopt( handle, CURLOPT_FOLLOWLOCATION, 1L );

    /** Should not take more than 30s to get the info */
    curl_easy_setopt( handle, CURLOPT_TIMEOUT,        30 );

    CURLcode ret = curl_easy_perform( handle );

    if ( ret != CURLE_OK ) {
        emit paperFailed( mDoi );
        curl_easy_cleanup( handle );

        return;
    }

    /**
     * Simplyfy the string
     *  .simplified() replaces multiple spaces and various whitespaces by a single space \x20.
     *  .replace(...) ensures that we can search for id="pdf", src="(.*)" safely.
     */
    response = response.simplified().replace( " = ", "=" ).replace( "'", "\"" );

    /** This is for embed version (which we get from libcurl) */
    QRegularExpression rx1(
        "<embed type=\"application/pdf\" src=\"(.*)\" id=\"pdf\">",
        QRX::DotMatchesEverythingOption | QRX::DotMatchesEverythingOption
    );

    /** This is for iframe version (which we get from curl) */
    QRegularExpression rx2(
        "<iframe src=\"(.*)\" id=\"pdf\">",
        QRX::DotMatchesEverythingOption | QRX::DotMatchesEverythingOption
    );

    QRegularExpressionMatch m1 = rx1.match( response );
    QRegularExpressionMatch m2 = rx2.match( response );

    pdf = (m1.hasMatch() ? m1.captured( 1 ) : m2.captured( 1 ) );

    if ( not pdf.size() ) {
        emit paperFailed( mDoi );
        return;
    }

    if ( pdf.startsWith( "//" ) ) {
        pdf = "https:" + pdf;
    }

    else if ( pdf.startsWith( "/" ) ) {
        pdf = "https://sci-hub.ee" + pdf;
    }

    QFile file( "/tmp/" + QString( mDoi ).replace( "/", "_" ) + ".pdf" );
    file.open( QFile::WriteOnly );

    curl_easy_reset( handle );
    curl_easy_setopt( handle, CURLOPT_URL, pdf.toUtf8().data() );
    curl_easy_setopt(
        handle, CURLOPT_WRITEFUNCTION, +[] (char *buffer, size_t size, size_t nmemb, QFile * file)->size_t {
            size_t newLength = size * nmemb;
            try {
                file->write( buffer, newLength );
                file->flush();
            }catch ( std::bad_alloc& e ) {
                return 0;
            }

            return newLength;
        }
    );

    curl_easy_setopt( handle, CURLOPT_WRITEDATA,      &file );
    curl_easy_setopt( handle, CURLOPT_FOLLOWLOCATION, 1L );

    /** Should not take more than 5m to get the paper */
    curl_easy_setopt( handle, CURLOPT_TIMEOUT,        300 );

    ret = curl_easy_perform( handle );
    curl_easy_cleanup( handle );

    file.close();

    if ( ret != CURLE_OK ) {
        emit paperFailed( mDoi );
    }

    else {
        /** Check if the download was proper. */
        QMimeType mt = mimeDb.mimeTypeForFile( file.fileName(), QMimeDatabase::MatchContent );

        if ( mt.name() == "application/pdf" ) {
            emit paperReady( mDoi, file.fileName() );
        }

        else {
            emit paperFailed( mDoi );
        }
    }
}


void CurlRunner::fetchBibTex() {
    handle = curl_easy_init();

    QString response;
    QString api = QString( "https://api.crossref.org/v1/works/%1/transform/application/x-bibtex" ).arg( mDoi );

    curl_easy_setopt( handle, CURLOPT_URL, api.toUtf8().data() );
    curl_easy_setopt(
        handle, CURLOPT_WRITEFUNCTION, +[] (char *buffer, size_t size, size_t nmemb, QString * str)->size_t {
            size_t newLength = size * nmemb;
            try {
                str->append( QString::fromUtf8( buffer, newLength ) );
            }catch ( std::bad_alloc& e ) {
                return 0;
            }

            return newLength;
        }
    );

    curl_easy_setopt( handle, CURLOPT_WRITEDATA, &response );
    /** Should not take more than 30s to get the info */
    curl_easy_setopt( handle, CURLOPT_TIMEOUT,   30 );

    /** Perform the request */
    CURLcode ret = curl_easy_perform( handle );

    /** Cleanup the handle */
    curl_easy_cleanup( handle );

    /** Emit the signal only if we recieved bibtex data */
    if ( ret == CURLE_OK ) {
        if ( (not response.isEmpty() ) and (response != "Resource not found.") ) {
            emit bibReady( mDoi, response );
            return;
        }
    }

    /** Otherwise state that we have failed */
    emit bibFailed( mDoi );
}


void CurlRunner::run() {
    switch ( mWhat ) {
        /** Info Only */
        case 0x01: {
            getPaperInfo();
            emit paperFailed( mDoi );
            emit bibFailed( mDoi );
            break;
        }

        /** PDF Only */
        case 0x02: {
            fetchPaper();
            emit infoFailed( mDoi );
            emit bibFailed( mDoi );
            break;
        }

        /** Info and PDF */
        case 0x03: {
            getPaperInfo();
            fetchPaper();
            emit bibFailed( mDoi );
            break;
        }

        /** BibTex Only */
        case 0x04: {
            fetchBibTex();
            emit infoFailed( mDoi );
            emit paperFailed( mDoi );
            break;
        }

        /** Info and BibTex */
        case 0x05: {
            getPaperInfo();
            fetchBibTex();
            emit paperFailed( mDoi );
            break;
        }

        /** PDF and BibTex */
        case 0x06: {
            fetchPaper();
            fetchBibTex();
            emit infoFailed( mDoi );
            break;
        }

        /** All */
        case 0x07: {
            [[fallthrough]];
        }

        default: {
            getPaperInfo();
            fetchPaper();
            fetchBibTex();
        }
    }
}


PaperSearch::PaperSearch( QObject *parent ) : QObject( parent ) {
    curl_global_init( CURL_GLOBAL_ALL );
    QThreadPool::globalInstance()->setMaxThreadCount( 10 );
}


void PaperSearch::requestPaper( QString newdoi, int what ) {
    CurlRunner *request = new CurlRunner( newdoi, what );

    connect(
        request, &CurlRunner::infoReady, [ this ] ( QString doi, QString info ) {
            infoHash.insert( doi, info );
            qDebug() << "Info recieved:" << doi;

            emitIfReady( doi );
        }
    );

    connect(
        request, &CurlRunner::infoFailed, [ this ] ( QString doi ) {
            infoHash.insert( doi, "" );
            qDebug() << "Info failed:" << doi;

            emitIfReady( doi );
        }
    );

    connect(
        request, &CurlRunner::paperReady, [ this ] ( QString doi, QString path ) {
            pathHash.insert( doi, path );
            qDebug() << "Paper recieved:" << doi;

            emitIfReady( doi );
        }
    );

    connect(
        request, &CurlRunner::paperFailed, [ this ] ( QString doi ) {
            pathHash.insert( doi, "" );
            qDebug() << "Paper failed:" << doi;

            emitIfReady( doi );
        }
    );

    connect(
        request, &CurlRunner::bibReady, [ this ] ( QString doi, QString path ) {
            bbtxHash.insert( doi, path );
            qDebug() << "Bib recieved:" << doi;

            emitIfReady( doi );
        }
    );

    connect(
        request, &CurlRunner::bibFailed, [ this ] ( QString doi ) {
            bbtxHash.insert( doi, "" );
            qDebug() << "Bib failed:" << doi;

            emitIfReady( doi );
        }
    );

    QThreadPool::globalInstance()->start( request );
}


void PaperSearch::emitIfReady( QString doi ) {
    bool ready = true;

    ready &= infoHash.contains( doi );
    ready &= pathHash.contains( doi );
    ready &= bbtxHash.contains( doi );

    QString data = infoHash.value( doi ) + pathHash.value( doi ) + bbtxHash.value( doi );

    if ( ready ) {
        if ( not data.size() ) {
            emit searchFailed( doi );
            return;
        }

        emit paperDownloaded(
            doi,
            infoHash.value( doi ),
            pathHash.value( doi ),
            bbtxHash.value( doi )
        );
    }
}
