/**
 * This file is a part of Papyrus.
 * A Research Paper Manager
 * Copyright 2018-2022 Marcus Britanicus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, vsit http://www.gnu.org/licenses/.
 **/

#include <QCryptographicHash>

#include "PaperInfo.hpp"

PaperInfo::PaperInfo( QString hash ) {
    /* Already existing paper */
    if ( papersDB->childGroups().contains( hash ) ) {
        mHash = QString( hash );
    }

    /* Newly added paper: hash will be Pdf Path */
    else {
        /* A randomly generated hash */
        mHash = QCryptographicHash::hash( QDateTime::currentDateTime().toString().toLocal8Bit(), QCryptographicHash::Md5 ).toHex();
        setPdfPath( hash );
    }
}


QString PaperInfo::pdfPath() {
    return papersDB->value( mHash + "/Path" ).toString();
}


void PaperInfo::setPdfPath( QString newPath ) {
    papersDB->setValue( mHash + "/Path", newPath );
}


QString PaperInfo::title() {
    return papersDB->value( mHash + "/Title" ).toString();
}


void PaperInfo::setTitle( QString newTitle ) {
    papersDB->setValue( mHash + "/Title", newTitle );
}


QString PaperInfo::plainTitle() {
    return QTextDocumentFragment::fromHtml( papersDB->value( mHash + "/Title" ).toString() ).toPlainText();
}


QStringList PaperInfo::authors() {
    return papersDB->value( mHash + "/Authors" ).toStringList();
}


void PaperInfo::setAuthors( QStringList newAuthors ) {
    papersDB->setValue( mHash + "/Authors", newAuthors );
}


QDate PaperInfo::date() {
    return papersDB->value( mHash + "/Date" ).toDate();
}


void PaperInfo::setDate( QDate newDate ) {
    papersDB->setValue( mHash + "/Date", newDate );
}


QStringList PaperInfo::tags() {
    return papersDB->value( mHash + "/Tags" ).toStringList();
}


void PaperInfo::setTags( QStringList newTags ) {
    papersDB->setValue( mHash + "/Tags", newTags );
}


QString PaperInfo::abstract() {
    return papersDB->value( mHash + "/Abstract" ).toString();
}


void PaperInfo::addAbstract( QString newAbstract ) {
    papersDB->setValue( mHash + "/Abstract", newAbstract );
}


QString PaperInfo::notes() {
    return papersDB->value( mHash + "/Notes" ).toString();
}


void PaperInfo::addNotes( QString newNotes ) {
    papersDB->setValue( mHash + "/Notes", newNotes );
}
