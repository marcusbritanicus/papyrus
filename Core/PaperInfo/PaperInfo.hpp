/**
 * This file is a part of Papyrus.
 * A Research Paper Manager
 * Copyright 2018-2022 Marcus Britanicus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, vsit http://www.gnu.org/licenses/.
 **/

#pragma once

#include "Global.hpp"

class PaperInfo {
    public:
        PaperInfo() {}
        PaperInfo( QString );

        /* Path */
        QString pdfPath();
        void setPdfPath( QString );

        /* Paper Title */
        QString title();
        void setTitle( QString );

        /* Title with all html tags removed */
        QString plainTitle();

        /* Authors */
        QStringList authors();
        void setAuthors( QStringList );

        /* Date of publication */
        QDate date();
        void setDate( QDate );

        /* Tags */
        QStringList tags();
        void setTags( QStringList );

        /* Abstract */
        QString abstract();
        void addAbstract( QString );

        /* Notes */
        QString notes();
        void addNotes( QString );

        /* Hash */
        inline QString hash() {
            return mHash;
        }

    private:
        /* MD5Hash( mTitle + mAuthors.join( "" ) )  */
        QString mHash;
};

Q_DECLARE_METATYPE( PaperInfo );
