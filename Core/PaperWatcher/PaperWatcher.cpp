/**
 * This file is a part of Papyrus.
 * A Research Paper Manager
 * Copyright 2018-2022 Marcus Britanicus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, vsit http://www.gnu.org/licenses/.
 **/

#include <QtCore>

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <locale.h>
#include <iostream>
#include <list>

#include "PaperWatcher.hpp"

#include <sys/inotify.h>

#define MAX_EVENTS    1024
#define EVENT_SIZE    (sizeof(struct inotify_event) )
#define BUF_LEN       (MAX_EVENTS * (EVENT_SIZE + NAME_MAX + 1) )

PaperWatcher::PaperWatcher() : QThread() {
    inotifyFD = inotify_init();

    if ( inotifyFD < 0 ) {
        qCritical() << "Failed initialize inotify";
        inotifyFailed();
    }

    expireTimer = new QBasicTimer();
    expireTimer->start( 50, Qt::PreciseTimer, this );
}


PaperWatcher::~PaperWatcher() {
    __stopWatch = true;

    expireTimer->stop();
    delete expireTimer;

    // We should now close all the open watches
    for ( int key: wdPathHash.keys() ) {
        wdPathHash.remove( key );
        wdModeHash.remove( key );
        inotify_rm_watch( inotifyFD, key );
    }

    // Close the inotify instance
    close( inotifyFD );
}


void PaperWatcher::addWatch( QString wPath, Mode wMode ) {
    /**
     * Remove the trailing '/' from paths
     */
    while ( (wPath != "/") and wPath.endsWith( "/" ) ) {
        wPath.chop( 1 );
    }

    // Add a new watch
    int WD = inotify_add_watch( inotifyFD, wPath.toUtf8().data(), IN_ALL_EVENTS );

    if ( WD == -1 ) {
        qWarning() << "Couldn't add watch: " << wPath.toUtf8().constData();
        emit watchFailed( wPath );
    }

    wdPathHash[ WD ] = wPath;
    wdModeHash[ WD ] = (QFileInfo( wPath ).isDir() ? wMode : PathOnly);

    /**
     * Add more watches based on wMode
     */
    if ( QFileInfo( wPath ).isDir() ) {
        if ( wMode == Recursive ) {
            QDirIterator it( wPath, QDir::AllEntries | QDir::NoDotAndDotDot | QDir::System | QDir::Hidden, QDirIterator::Subdirectories );
            while ( it.hasNext() ) {
                QString entry( it.next() );
                WD = inotify_add_watch( inotifyFD, entry.toUtf8().data(), IN_ALL_EVENTS );

                if ( WD != -1 ) {
                    wdPathHash[ WD ] = entry;
                    wdModeHash[ WD ] = (it.fileInfo().isDir() ? Recursive : PathOnly);
                }
            }
        }

        else if ( wMode == Contents ) {
            QDirIterator it( wPath, QDir::Files | QDir::System | QDir::Hidden );
            while ( it.hasNext() ) {
                QString entry( it.next() );
                WD = inotify_add_watch( inotifyFD, entry.toUtf8().data(), IN_ALL_EVENTS );

                if ( WD != -1 ) {
                    wdPathHash[ WD ] = entry;
                    wdModeHash[ WD ] = PathOnly;
                }
            }
        }
    }
}


void PaperWatcher::removeWatch( QString path ) {
    for ( int key: wdPathHash.keys() ) {
        if ( wdPathHash.value( key ).startsWith( path ) ) {
            wdPathHash.remove( key );
            wdModeHash.remove( key );
            inotify_rm_watch( inotifyFD, key );
        }
    }
}


void PaperWatcher::startWatch() {
    __stopWatch = false;
    start();
}


void PaperWatcher::stopWatch() {
    __stopWatch = true;
}


void PaperWatcher::run() {
    while ( not __stopWatch ) {
        int length = 0, i = 0;

        while ( true ) {
            char buffer[ BUF_LEN ] = { 0 };
            length = read( inotifyFD, buffer, BUF_LEN );

            i = 0;

            // If for some reason length < 0, continue with trying to read.
            if ( length < 0 ) {
                continue;
            }

            while ( i < length ) {
                struct inotify_event *event = ( struct inotify_event * )&buffer[ i ];

                /* WD of the current event */
                int curWD = event->wd;

                // If this wd does not exist in our hash
                if ( not wdPathHash.keys().contains( curWD ) ) {
                    break;
                }

                QString node = wdPathHash.value( curWD );
                QString path = node + (event->len ? QString( "/%1" ).arg( event->name ) : "");

                /**
                 * IN_DELETE_SELF means the node which was being watched was deleted
                 * So we should delete it from the
                 */
                if ( event->mask & IN_DELETE_SELF ) {
                    emit paperDeleted( node );
                    removeWatch( node );
                }

                // New file was created inside one of the directories we were watching
                else if ( (event->mask & IN_CREATE) ) {
                    emit paperAdded( path );
                    switch ( wdModeHash.value( curWD ) ) {
                        case Recursive: {
                            addWatch( path, Recursive );
                            break;
                        }

                        case Contents: {
                            addWatch( path, Contents );
                            break;
                        }

                        default: {
                            break;
                        }
                    }
                }

                // A file was modified inside one of the directories we were watching
                else if ( event->mask & IN_MODIFY ) {
                    /**
                     * If we have a folder, and we're watching it with PathOnly mode,
                     * then is the only chance to intimate the user that the file was modified
                     */
                    if ( QFileInfo( node ).isDir() and wdModeHash.value( curWD ) == PathOnly ) {
                        emit paperChanged( path );
                    }

                    /**
                     * Else if, we have a file, we will right away intimate the user
                     * that the file was modified
                     */
                    else if ( not QFileInfo( node ).isDir() ) {
                        emit paperChanged( node );
                    }
                }

                // A file was deleted inside one of the directories we were watching
                else if ( (event->mask & IN_DELETE) ) {
                    emit paperDeleted( path );
                    removeWatch( path );
                }

                // A file was moved FROM one of the directories we were watching
                else if ( (event->mask & IN_MOVED_FROM) ) {
                    cookiePathHash[ event->cookie ] = path;
                    cookieTimeHash[ event->cookie ] = QDateTime::currentMSecsSinceEpoch();

                    /**
                     * Remove this path from watch and put it into pending renames
                     */
                    removeWatch( path );

                    /**
                     * Further watch of this node will happen only if this directory was
                     * watched in Recursive or Contents mode.
                     */
                    switch ( wdModeHash.value( curWD ) ) {
                        case Recursive: {
                            pendingRenames[ event->cookie ] = Recursive;
                            break;
                        }

                        case Contents: {
                            /* If we have a folder, ignore it */
                            if ( not QFileInfo( path ).isDir() ) {
                                pendingRenames[ event->cookie ] = PathOnly;
                            }

                            break;
                        }

                        case PathOnly:
                        default: {
                            break;
                        }
                    }
                }

                // A file was moved TO one of the directories we were watching
                else if ( (event->mask & IN_MOVED_TO) ) {
                    // Previously, we received a IN_MOVED_FROM event with this cookie.
                    if ( cookiePathHash.keys().contains( event->cookie ) ) {
                        /** Add a watch and then emit renamed signal */
                        addWatch( path, pendingRenames.value( event->cookie ) );
                        emit paperRenamed( cookiePathHash.value( event->cookie ), path );

                        cookiePathHash.remove( event->cookie );
                        cookieTimeHash.remove( event->cookie );
                        pendingRenames.remove( event->cookie );
                    }

                    // This node is being moved from somewhere outside our watches
                    else {
                        addWatch( path, wdModeHash.value( curWD ) );
                        emit paperAdded( path );
                    }
                }

                // A file/directory we were watching was moved
                else if ( (event->mask & IN_MOVE_SELF) ) {
                    // We will remove the watch, and create a delete event
                    emit paperDeleted( node );
                    wdPathHash.remove( curWD );
                    wdModeHash.remove( curWD );

                    removeWatch( node );
                }

                i += EVENT_SIZE + event->len;
            }
        }
    }
}


void PaperWatcher::timerEvent( QTimerEvent *tEvent ) {
    if ( expireTimer->timerId() == tEvent->timerId() ) {
        time_t curTime = QDateTime::currentMSecsSinceEpoch();
        for ( int cookie: cookieTimeHash.keys() ) {
            if ( curTime - cookieTimeHash.value( cookie ) > 500 ) {
                QString path = cookiePathHash.value( cookie );
                emit    paperDeleted( path );

                cookiePathHash.remove( cookie );
                cookieTimeHash.remove( cookie );
                pendingRenames.remove( cookie );
            }
        }
    }

    QThread::timerEvent( tEvent );
}
