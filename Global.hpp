/**
 * This file is a part of Papyrus.
 * A Research Paper Manager
 * Copyright 2018-2022 Marcus Britanicus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, vsit http://www.gnu.org/licenses/.
 **/

#pragma once

#include <QtCore>
#include <QtGui>
#include <QtWidgets>

#include "PaperSearch.hpp"

/* DatabasePath - Place where we store the paper info */
extern QSettings   *papSett;
extern QString     dbPath;
extern QStringList papersPath;

extern QSettings *papersDB;

/* QVariantList typedef */
typedef QList<QVariant> QVariantList;

/* QMimeTypeDatabse */
static QMimeDatabase mimeDb;

/* InfoSearcher */
extern PaperSearch *doiSearch;

/**
 * Chemical Fourmula Regex
 * [A-Z][a-z]?([0-9]*[.])?[0-9]*|\((?:[^()]*(?:\(.*\))?[^()]*)+\)([0-9]*[.])?[0-9]*
 */
