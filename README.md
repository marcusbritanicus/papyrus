# Papyrus

A simple research paper manager based on Qt5/C++ with Poppler-Qt5 backend


### Notes for compiling (Qt5) - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/marcusbritanicus/Papyrus.git Papyrus`
- Enter the `Papyrus` folder
  * `cd Papyrus`
- Configure the project - we use meson for project management
  * `meson .build --prefix=/usr --buildtype=release`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`


### Dependencies
  * Qt5 (qtbase5-dev, qtbase5-dev-tools)
  * QDocumentView (https://gitlab.com/marcusbritanicus/qdocumentview)
  * Poppler Qt
  * [DFL::IPC](https://gitlab.com/desktop-frameworks/ipc)
  * [DFL::Application](https://gitlab.com/desktop-frameworks/applications)